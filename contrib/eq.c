/*
    Copyright (C) 2007 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
/*
 * A equalizer plugin for mpg123. Saves the settings to ~/.ncast/eq.conf.
 *
 * gcc -O2 -g -Wall -shared -o eq.so eq.c
 */
#include <stdio.h>
#include <curses.h>
#include <panel.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include <pwd.h>

#define HEIGHT 16
#define WIDTH 71
#define INCR		0.25
#define LEVEL_MAX	3.00
#define DEFAULT_LEVEL	1.00

#define FREQ2POS(f)	(int)(HEIGHT - 3 - (f.l / INCR))
#define RANGE_STRING	"     [       LOW       ] [       MIDDLE       ] [       HIGH       ]"
#define CTRL(x)         ((x) & 0x1f)

static struct eq_value_s {
    float l;
    float r;
} eq_values[32];

static struct eq_value_s eq_values_orig[32];
static int selected;
static char filename[PATH_MAX];
typedef void(*ncast_player_t)(const char *fmt, ...);

static void init_colors()
{
    if (has_colors() == TRUE) {
	if (start_color() != ERR) {
	    /* 
	     * The colors shouldn't conflict with ncast or other plugins.
	     */
	    init_pair(62, COLOR_WHITE, COLOR_BLUE);
	    init_pair(63, COLOR_YELLOW, COLOR_MAGENTA);
	}
    }
}

static void read_conf()
{
    char line[LINE_MAX];
    char *p;
    int n = 0;
    FILE *fp;
    struct passwd *pw = getpwuid(getuid());

    snprintf(filename, sizeof(filename), "%s/.ncast/mpg123-eq.conf", pw->pw_dir);

    if ((fp = fopen(filename, "r")) == NULL)
	return;

    while ((p = fgets(line, sizeof(line), fp)) != NULL) {
	char l[16], r[16];
	float f;

	if (p[strlen(p) - 1] == '\n')
	    p[strlen(p) - 1] = 0;

	if (sscanf(p, "%s %s", l, r) != 2) {
	    n += 2;
	    continue;
	}

	f = atof(l);

	if (f < 0.00 || f > LEVEL_MAX) {
	    n++;
	    continue;
	}

	eq_values[n].l = atof(l);
	f = atof(r);

	if (f < 0.00 || f > LEVEL_MAX) {
	    n++;
	    continue;
	}

	eq_values[n++].r = atof(r);
    }

    fclose(fp);
}

static void init_eq_values()
{
    int i;

    for (i = 0; i < 32; i++)
	eq_values[i].l = eq_values[i].r = 1.00;
}

char *ncast_plugin_init(char **args)
{
    char buf[LINE_MAX];

    init_eq_values();
    read_conf();
    snprintf(buf, sizeof(buf), "-E %s", filename);
    *args = strdup(buf);
    return "mpg123 equalizer";
}

static void draw_channel(WINDOW *w, int x, int channel)
{
    int i, n;

    n = FREQ2POS(eq_values[channel]);

    for (i = 1; i < HEIGHT-2; i++) {
	chtype c = ACS_VLINE;

	if (i == n)
	    c = ACS_DIAMOND;

	mvwaddch(w, i, x, selected == channel ? c | COLOR_PAIR(63) | A_BOLD : c);
    }
}

static void save_eq()
{
    FILE *fp;
    int i;

    if ((fp = fopen(filename, "w+")) == NULL)
	return;

    for (i = 0; i < 32; i++)
	fprintf(fp, "%.2f %.2f\n", eq_values[i].l, eq_values[i].r);

    fclose(fp);
}

static void update_mpg123(ncast_player_t player, int channel)
{
    player("EQ %i %i %.2f\n", 0, channel, eq_values[channel].l);
    player("EQ %i %i %.2f\n", 1, channel, eq_values[channel].r);
}

static void adjust_channel(int which, int channel)
{
    /*
     * increase
     */
    if (which == 1) {
	if (eq_values[channel].l + INCR <= LEVEL_MAX)
	    eq_values[channel].l += INCR;

	if (eq_values[channel].r + INCR <= LEVEL_MAX)
	    eq_values[channel].r += INCR;

	return;
    }

    if (eq_values[channel].l - INCR < 0)
	eq_values[channel].l = 0;
    else
	eq_values[channel].l -= INCR;

    if (eq_values[channel].r - INCR < 0)
	eq_values[channel].r = 0;
    else
	eq_values[channel].r -= INCR;

}

void ncast_plugin(ncast_player_t player)
{ 
    WINDOW *w;
    PANEL *pan;
    int curs;
    int channel = 0;

    w = newwin(HEIGHT, WIDTH, LINES / 2 - HEIGHT / 2, COLS / 2 - WIDTH / 2);
    pan = new_panel(w);
    keypad(w, TRUE);
    curs = curs_set(0);
    noecho();
    init_colors();
    memcpy(eq_values_orig, eq_values, sizeof(struct eq_value_s) * 32);

    while (1) {
	int c;
	int i;
	int x = 1;
	float f = 0;

	wattron(w, COLOR_PAIR(62) | A_BOLD);

	for (i = 0; i < HEIGHT; i++) {
	    for (c = 0; c < WIDTH; c++) {
		mvwaddch(w, i, c, ' ');
	    }
	}

	for (i = HEIGHT - 3; i >= 1; i--) {
	    mvwprintw(w, i, 1, "%.2f", f);
	    f += 0.25;
	}

	box(w, ACS_VLINE, ACS_HLINE);
	mvwprintw(w, 0, WIDTH / 2 - strlen("[ mpg123 equalizer ]") / 2, "[ mpg123 equalizer ]");
	selected = channel;

	for (x = 6, i = 0; i < 32; i++, x++)
	    draw_channel(w, x++, i);

	mvwprintw(w, HEIGHT-2, 1, "%s", RANGE_STRING);
	wmove(w, FREQ2POS(eq_values[channel]), channel+1+5);
	update_panels();
	doupdate();
	c = wgetch(w);

	switch (c) {
	    case CTRL('['):
		memcpy(eq_values, eq_values_orig, sizeof(struct eq_value_s) * 32);
		goto done;
	    case 'R':
		endwin();
		break;
	    case '=':
		eq_values[channel].l = eq_values[channel].r = DEFAULT_LEVEL;
		update_mpg123(player, channel);
		break;
	    case KEY_RIGHT:
		channel++;

		if (channel > 31)
		    channel = 0;
		break;
	    case KEY_LEFT:
		channel--;

		if (channel < 0)
		    channel = 31;
		break;
	    case KEY_UP:
		adjust_channel(1, channel);
		update_mpg123(player, channel);
		break;
	    case KEY_DOWN:
		adjust_channel(0, channel);
		update_mpg123(player, channel);
		break;
	    case 'q':
		save_eq();
		goto done;
	    default:
		break;
	}
    }

done:
    hide_panel(pan);
    update_panels();
    doupdate();
    del_panel(pan);
    delwin(w);
    curs_set(curs);
}
