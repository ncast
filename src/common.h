/* vim:tw=78:ts=8:sw=4:set ft=c:  */
/*
    Copyright (C) 2005 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef COMMON_H
#define COMMON_H

enum {
    SORT_PLAYING, SORT_BITS, SORT_DESC, SORT_USER, SORT_LIMIT, SORT_RANK,
    SORT_MAX
};

struct plugin_s {
    void *h;			/* Plugin handle. */
    char *args;			/* Return value of the plugin. */
    struct binding_s *binding;	/* Key binding for this plugin. */
    int touched;		/* To keep from having to restart mplayer
				 * every time a url is selected. */
};

struct color_s {
    short fg;
    short bg;
    int attrs;
    int nattrs;
};

struct {
    int max_pages;
    int sort;
    int reverse_sort;
    int bmstartup;
    int nodups;
    struct plugin_s *plugins;
    int plugin;
    struct color_s color[16];
    int colors;
    int collapse;
} config;

#endif
