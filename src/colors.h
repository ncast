/* vim:tw=78:ts=8:sw=4:set ft=c:  */
/*
    Copyright (C) 2005 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef COLORS_H
#define COLORS_H

#define CP_GENRE_SELECTED	((COLORS) ? \
	COLOR_PAIR(1) | config.color[0].attrs : \
	config.color[0].nattrs)

#define CP_GENRE_OTHER ((COLORS) ? \
	COLOR_PAIR(2) | config.color[1].attrs : \
	config.color[1].nattrs)

#define CP_GENRE_SECTION ((COLORS) ? \
	COLOR_PAIR(3) | config.color[2].attrs : \
	config.color[2].nattrs)

#define CP_PICK_SELECTED ((COLORS) ? \
	COLOR_PAIR(4) | config.color[3].attrs : \
	config.color[3].nattrs)

#define CP_PICK_OTHER ((COLORS) ? \
	COLOR_PAIR(5) | config.color[4].attrs : \
	config.color[4].nattrs)

#define CP_ACTIVE_BORDER ((COLORS) ? \
	COLOR_PAIR(6) | config.color[5].attrs : \
	config.color[5].nattrs)

#define CP_INACTIVE_BORDER ((COLORS) ? \
	COLOR_PAIR(7) | config.color[6].attrs : \
	config.color[6].nattrs)

#define CP_SEARCH_WINDOW ((COLORS) ? \
	COLOR_PAIR(8) | config.color[7].attrs : \
	config.color[7].nattrs)

#define CP_STATUS_WINDOW ((COLORS) ? \
	COLOR_PAIR(9) | config.color[8].attrs : \
	config.color[8].nattrs)

#define CP_HELP_WINDOW ((COLORS) ? \
	COLOR_PAIR(10) | config.color[9].attrs : \
	config.color[9].nattrs)

#define CP_HELP_BORDER ((COLORS) ? \
	COLOR_PAIR(11) | config.color[10].attrs : \
	config.color[10].nattrs)

#define CP_SCROLL_ARROW ((COLORS) ? \
	COLOR_PAIR(12) | config.color[11].attrs : \
	config.color[11].nattrs)

#define CP_BOOKMARK ((COLORS) ? \
	COLOR_PAIR(13) | config.color[12].attrs : \
	config.color[12].nattrs)

#define CP_LINE_ART ((COLORS) ? \
	COLOR_PAIR(14) | config.color[13].attrs : \
	config.color[13].nattrs)

#define CP_PROGRESS ((COLORS) ? \
	COLOR_PAIR(15) | config.color[14].attrs : \
	config.color[14].nattrs)

#define CP_GENRE_PICKS ((COLORS) ? \
	COLOR_PAIR(16) | config.color[15].attrs : \
	config.color[15].nattrs)

#endif
