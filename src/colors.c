/* vim:tw=78:ts=8:sw=4:set ft=c:  */
/*
    Copyright (C) 2005 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_NCURSES_H
#include <ncurses.h>
#endif

#include "common.h"
#include "colors.h"

void init_color_pairs()
{
    init_pair(1, config.color[0].fg, config.color[0].bg);
    init_pair(2, config.color[1].fg, config.color[1].bg);
    init_pair(3, config.color[2].fg, config.color[2].bg);
    init_pair(4, config.color[3].fg, config.color[3].bg);
    init_pair(5, config.color[4].fg, config.color[4].bg);
    init_pair(6, config.color[5].fg, config.color[5].bg);
    init_pair(7, config.color[6].fg, config.color[6].bg);
    init_pair(8, config.color[7].fg, config.color[7].bg);
    init_pair(9, config.color[8].fg, config.color[8].bg);
    init_pair(10, config.color[9].fg, config.color[9].bg);
    init_pair(11, config.color[10].fg, config.color[10].bg);
    init_pair(12, config.color[11].fg, config.color[11].bg);
    init_pair(13, config.color[12].fg, config.color[12].bg);
    init_pair(14, config.color[13].fg, config.color[13].bg);
    init_pair(15, config.color[14].fg, config.color[14].bg);
    init_pair(16, config.color[15].fg, config.color[15].bg);
}

void set_default_colors()
{
    config.color[0].fg = COLOR_CYAN;
    config.color[0].bg = COLOR_MAGENTA;
    config.color[0].attrs = A_BOLD;
    config.color[0].nattrs = A_REVERSE;
    config.color[1].fg = COLOR_WHITE;
    config.color[1].bg = COLOR_RED;
    config.color[2].fg = COLOR_YELLOW;
    config.color[2].bg = COLOR_RED;
    config.color[2].attrs = A_BOLD;
    config.color[2].nattrs = A_BOLD;
    config.color[3].fg = COLOR_CYAN;
    config.color[3].bg = COLOR_MAGENTA;
    config.color[3].attrs = A_BOLD;
    config.color[3].nattrs = A_REVERSE;
    config.color[4].fg = COLOR_GREEN;
    config.color[4].bg = COLOR_BLACK;
    config.color[5].fg = COLOR_GREEN;
    config.color[5].bg = COLOR_BLACK;
    config.color[5].attrs = A_BOLD;
    config.color[5].nattrs = A_BOLD;
    config.color[6].fg = COLOR_MAGENTA;
    config.color[6].bg = COLOR_BLACK;
    config.color[7].fg = COLOR_CYAN;
    config.color[7].bg = COLOR_YELLOW;
    config.color[7].attrs = A_BOLD;
    config.color[7].nattrs = A_BOLD;
    config.color[8].fg = COLOR_CYAN;
    config.color[8].bg = COLOR_BLUE;
    config.color[8].attrs = A_BOLD;
    config.color[8].nattrs = A_REVERSE;
    config.color[9].fg = COLOR_WHITE;
    config.color[9].bg = COLOR_YELLOW;
    config.color[9].attrs = A_BOLD;
    config.color[10].fg = COLOR_WHITE;
    config.color[10].bg = COLOR_YELLOW;
    config.color[11].fg = COLOR_CYAN;
    config.color[11].bg = COLOR_BLACK;
    config.color[11].attrs = A_BOLD;
    config.color[11].nattrs = A_BOLD;
    config.color[12].fg = COLOR_BLUE;
    config.color[12].bg = COLOR_RED;
    config.color[13].fg = COLOR_CYAN;
    config.color[13].bg = COLOR_RED;
    config.color[13].nattrs = A_BOLD;
    config.color[14].fg = COLOR_WHITE;
    config.color[14].bg = COLOR_GREEN;
    config.color[14].attrs = A_BOLD;
    config.color[14].nattrs = A_BOLD;
    config.color[15].fg = COLOR_WHITE;
    config.color[15].bg = COLOR_RED;
    config.color[15].attrs = A_BOLD;
    config.color[15].nattrs = A_BOLD;
}
