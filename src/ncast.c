/* vim:tw=78:ts=8:sw=4:set ft=c:  */
/*
    Copyright (C) 2005 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <err.h>
#include <string.h>
#include <regex.h>
#include <ctype.h>
#include <dlfcn.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/stat.h>
#include <paths.h>
#include <fcntl.h>
#include <pwd.h>
#include <stdarg.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/time.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_CURL_CURL_H
#include <curl/curl.h>
#endif

#ifdef HAVE_NCURSES_H
#include <ncurses.h>
#endif

#ifdef HAVE_PANEL_H
#include <panel.h>
#endif

#ifdef HAVE_FORM_H
#include <form.h>
#endif

#ifdef HAVE_PTH_H
#include <pth.h>
#endif

#include "common.h"
#include "colors.h"
#include "ncast.h"

#ifdef WITH_DMALLOC
#include <dmalloc.h>
#endif

/* The indent variable in the message structure is for indenting wrapped lines
 * (pick info for example). See ncast.h for more information.
 */
static void *do_message(void *data)
{
    struct message_s *msg = data;
    WINDOW *w;
    PANEL *pan;
    char **lines = NULL;
    char *p;
    int lindex = 0;
    int x = 0;
    int y;
    int width = 0;
    char buf[LINES * COLS];
    int i, n;
    int maxx = (COLS / 5) * 4;
    int maxy = (LINES / 5) * 4;
    int wrap = 0;
    int posy = 0;
    int curs = curs_set(0);

    if (!curses_initialized) {
	write(msg->fd, msg->line, strlen(msg->line));

	if (msg->line[strlen(msg->line) - 1] != '\n')
	    write(msg->fd, "\n", 1);

	free(msg->line);
	free(msg);
	return NULL;
    }

    for (p = buf, i = 0; msg->line[i]; i++) {
      last:
	if (msg->line[i] == 0 || msg->line[i] == '\n' || x >= maxx) {
	    *p = '\0';

	    if (x >= maxx) {	// Word wrapping.
		int q = (msg->leading) ? abs(strlen(buf) - msg->indent) :
		    strlen(buf);

		for (n = 0; *p != ' ' && *p != '\t' && q >= 0;
		     p--, q--, n++);

		if (q < 0) {
		    p += n;
		    i--;
		}
		else
		    i -= n;

		wrap = 2;
		*p = '\0';
	    }

	    lines = realloc(lines, (lindex + 1) * sizeof(char *));
	    lines[lindex] = malloc((wrap == 1) ? strlen(buf) + msg->indent + 1
				   : strlen(buf) + 1);
	    lines[lindex][0] = '\0';

	    if (wrap == 1) {
		p = lines[lindex];

		for (n = 0; n < msg->indent; n++)
		    *p++ = ' ';

		*p = '\0';
	    }

	    strcat(lines[lindex], buf);

	    if (width < strlen(lines[lindex]))
		width = strlen(lines[lindex]);

	    wrap--;
	    buf[0] = x = 0;
	    p = buf;
	    lindex++;

	    if (msg->line[i])
		continue;
	    else
		break;
	}

	*p++ = msg->line[i];
	x++;
    }

    *p = '\0';

    if (buf[0] != '\0')
	goto last;

    free(msg->line);
    free(msg);

    y = (lindex + 2 > maxy) ? maxy + 2 : lindex + 2;
    x = (width + 2 > maxx) ? maxx + 2 : width + 2;
    maxx = x - 2;
    maxy = y - 2;
    w = newwin(y, x, LINES / 2 - y / 2, COLS / 2 - x / 2);
    wbkgd(w, CP_HELP_WINDOW);
    keypad(w, TRUE);
    pan = new_panel(w);
    msg_box++;

    while (1) {
	chtype c;

	wmove(w, 0, 0);
	wclrtobot(w);
	wattron(w, CP_HELP_BORDER);
	box(w, ACS_VLINE, ACS_HLINE);
	wattroff(w, CP_HELP_BORDER);

	if (posy < 0)
	    posy = 0;
	else
	    posy = (posy > abs(lindex - maxy)) ? lindex - maxy : posy;

	for (i = posy, y = 1, n = 0; i < lindex && n < maxy; i++, n++)
	    mvwaddstr(w, y++, 1, lines[i]);

	// FIXME ACS_BLOCK (scrolling)
	if (lindex > maxy) {
	    mvwaddch(w, 1, maxx + 1, ACS_UARROW);

	    y = (posy + 1 / (maxy - 2));

	    for (i = 2; i < maxy; i++)
		mvwaddch(w, i, maxx + 1, (y == i) ? ACS_BLOCK : ACS_CKBOARD);

	    mvwaddch(w, maxy, maxx + 1, ACS_DARROW);
	}

	update_panels();
	doupdate();
	wtimeout(w, TIMEOUT);

	while ((c = wgetch(w)) == ERR) {
	    top_panel(pan);
	    pth_yield(NULL);
	}

	switch (c) {
	    case KEY_END:
		posy = abs(lindex - maxy);
		break;
	    case KEY_HOME:
		posy = 0;
		break;
	    case KEY_DOWN:
		posy++;
		break;
	    case KEY_UP:
		posy--;
		break;
	    case KEY_NPAGE:
		posy += maxy;
		break;
	    case KEY_PPAGE:
		posy -= maxy;
		break;
	    default:
		goto done;
	}
    }

  done:
    for (i = 0; i < lindex; i++)
	free(lines[i]);

    free(lines);
    del_panel(pan);
    delwin(w);
    curs_set(curs);
    msg_box--;
    return NULL;
}

static void _message(int fd, int indent, int leading, char *format, ...)
{
    va_list ap;
    struct message_s *msg = NULL;
    pth_t th;
    pth_attr_t attr;

#ifndef HAVE_VASPRINTF
    char line[COLS * LINES];
#else
    char *line;
#endif

    if ((msg = malloc(sizeof(struct message_s))) == NULL) {
	se_message(0, 0, "malloc(): %s", strerror(errno));
	return;
    }

    msg->indent = indent;
    msg->leading = leading;
    msg->fd = fd;

    va_start(ap, format);
#ifndef HAVE_VASPRINTF
    vsnprintf(line, sizeof(line), format, ap);
#else
    vasprintf(&line, format, ap);
#endif
    va_end(ap);

    msg->line = strdup(line);
#ifdef HAVE_VASPRINTF
    free(line);
#endif

    if (threads_initialized) {
	attr = pth_attr_new();
	pth_attr_set(attr, PTH_ATTR_PRIO, PTH_PRIO_MAX);

	if ((th = pth_spawn(attr, do_message, msg)) == NULL) {
	    se_message(0, 0, "%s: %i: pth_spawn() failed", __FILE__,
		       __LINE__);
	    free(msg->line);
	    free(msg);
	}
	else
	    pth_yield(th);

	pth_attr_destroy(attr);
    }
    else
	do_message(msg);
}

static char *itoa(int i)
{
    static char buf[12];

    snprintf(buf, sizeof(buf), "%i", i);
    return buf;
}

static void free_picks(struct pick_s *p, int total)
{
    int i;

    for (i = 0; i < total; i++) {
	if (p[i].url)
	    free(p[i].url);

	if (p[i].homepage)
	    free(p[i].homepage);

	if (p[i].desc)
	    free(p[i].desc);

	if (p[i].playing)
	    free(p[i].playing);
    }
}

static int genre_total()
{
    int i, n;

    for (i = n = 0; i < ginfo.total; i++) {
	if (!(genres[i].flags & GENRE_HIDDEN))
	    n++;
    }

    return n;
}

static int genre_current()
{
    int i, n;

    for (i = n = 0; i < ginfo.y; i++) {
	if (!(genres[i].flags & GENRE_HIDDEN) || (genres[i].flags & GENRE_SECTION))
	    n++;
    }

    return n + 1;
}

static void update_status(struct genre_s *g)
{
    int i, x, n;
    char buf[COLS];

    if (g && status.state != BOOKMARKS) {
	if (g->flags & GENRE_CONNECT)
	    status.state = CONNECT;
	else if (g->flags & GENRE_DONE)
	    status.state = DONE;
	else if (!(g->flags & GENRE_DONE) && g->pages > 0)
	    status.state = FETCH;
	else
	    status.state = DONE;
    }

    wbkgd(status_w, CP_STATUS_WINDOW);

    for (i = 0; i < COLS; i++)
	mvwaddch(status_w, 0, i, ' ');

    if (g)
	mvwprintw(status_w, 0, 0, "Filter: %-8s  Pages: %-2i  Xfers: %-2i  Fetch: %-3s",
		  (g->nodups) ? "song" : "none", g->max_pages, nhandles, 
		  (g->flags & GENRE_AUTOFETCH) ? "yes" : "no");
    else
	mvwprintw(status_w, 0, 0, "Filter: %-8s  Pages: %-2i  Xfers: %-2i  Fetch: %-3s",
		  (config.nodups) ? "song" : "none", config.max_pages,
		  nhandles, "no");

    switch (status.state) {
	case CONNECT:
	    mvwprintw(status_w, 0, COLS - 13, "%s", "Connecting...");
	    break;
	case FETCH_GENRES:
	    snprintf(buf, sizeof(buf), "Fetching genres...");
	    mvwprintw(status_w, 0, COLS - strlen(buf), "%s", buf);
	    break;
	case FETCH:
	    snprintf(buf, sizeof(buf), "Fetching page %i of %s%s%i", g->page,
		     (g->pages > g->max_pages
		      && g->max_pages) ? itoa(g->max_pages) : "",
		     (g->pages > g->max_pages
		      && g->max_pages) ? "/" : "", g->pages);

	    n = (g->pages >=
		 g->max_pages) ? (g->max_pages) ? g->max_pages : g->
		pages : g->pages;

	    for (i = 0, x = COLS - strlen(buf); i < strlen(buf); i++, x++) {
		if (x - (COLS - strlen(buf)) < (strlen(buf) / n) * g->page)
		    mvwaddch(status_w, 0, x, buf[i] | CP_PROGRESS);
		else
		    mvwaddch(status_w, 0, x, buf[i]);
	    }
	    break;
	case BOOKMARKS:
	    snprintf(buf, sizeof(buf), "Bookmark %i of %i", g->by + 1, g->bookmark);
	    mvwprintw(status_w, 0, COLS - strlen(buf), "%s", buf);
	    break;
	case DONE:
	    snprintf(buf, sizeof(buf), "%s %i of %i%s%s%s",
		     (focus == GENRE) ? "Genre" : "Pick",
		     (focus == GENRE) ? genre_current() : g->y + 1,
		     (focus == GENRE) ? genre_total() : g->pick,
		     (g->pick && focus == GENRE) ? " - " : "",
		     (g->pick && focus == GENRE) ? itoa(g->pick) : "",
		     (g->pick
		      && focus == GENRE) ? (g->pick ==
					    1) ? " Pick" : " Picks" : "");
	    mvwprintw(status_w, 0, COLS - strlen(buf), "%s", buf);
	    break;
	default:
	    break;
    }
}

static void usage(char *pn, FILE * fp)
{
    fprintf(fp, "Usage: %s [-hvb] [-u <url>[:port]] [-p "
	    "<plugin>:<key>[:<desc>]] [-f <file>]\n", pn);
    fprintf(fp, "  -u    Alternate URL (%s)\n", SHOUTCAST_URL);
    fprintf(fp, "  -p    Load a plugin binding it to <key> with optional "
	    "<desc>\n");
    fprintf(fp, "  -f    Alternate configuration file (~/.ncast/config)\n");
    fprintf(fp, "  -b    Load bookmarks on startup (overrides configuration "
	    "value)\n");
    fprintf(fp, "  -h    This help text\n");
    fprintf(fp, "  -v    Version information\n");
    exit((fp == stdout) ? EXIT_SUCCESS : EXIT_FAILURE);
}

static void free_genre_data_once(struct genre_s *g)
{
    if (g->ch)
	curl_easy_cleanup(g->ch);

    if (g->urldata)
	free(g->urldata);

    if (g->pick) {
	free_picks(g->picks, g->pick);
	free(g->picks);
    }

    if (g->bookmark) {
	free_picks(g->bookmarks, g->bookmark);
	free(g->bookmarks);
    }

    free(g->title);

    if (g->url)
	free(g->url);
}

static void free_genres_data()
{
    int i;

    for (i = 0; i < ginfo.total; i++)
	free_genre_data_once(&genres[i]);

    ginfo.total = 0;
}

static void cleanup()
{
    int i;

    if (genres) {
	free_genres_data();
	free(genres);
    }

    if (plugin) {
	for (i = 0; i < plugin; i++) {
	    ncast_plugin_exit *e;

	    if ((e = dlsym(plugins[i].h, "ncast_plugin_exit")) != NULL)
		(*e) ();

	    dlclose(plugins[i].h);

	    if (plugins[i].binding[0].desc) {
		free(plugins[i].binding[0].desc);
		free(plugins[i].binding);
	    }

	    if (plugins[i].args)
		free(plugins[i].args);
	}

	free(plugins);
    }

    regfree(&genre_r);
    regfree(&pick_url_r);
    regfree(&pick_desc_r);
    regfree(&pick_play_r);
    regfree(&pick_page_r);
    regfree(&pick_user_r);
    regfree(&pick_bits_r);
    regfree(&pick_rank_r);

    del_panel(genre_p);
    del_panel(pick_p);
    del_panel(status_p);
    delwin(genre_w);
    delwin(pick_w);
    delwin(status_w);

    if (cookie)
	free(cookie);
}

static void add_pick(struct pick_s **p, int *idx, struct pick_s new)
{
    struct pick_s *dst = *p;
    int i = *idx;

    if ((dst = realloc(dst, (i + 1) * sizeof(struct pick_s))) == NULL) {
	se_message(0, 0, "%s: %i: realloc(): %s", __FILE__, __LINE__,
		   strerror(errno));
	return;
    }

    memcpy(&dst[i], &new, sizeof(struct pick_s));
    dst[i].playing = strdup(new.playing);
    dst[i].desc = strdup(new.desc);
    dst[i].homepage = strdup(new.homepage);
    dst[i++].url = strdup(new.url);
    *idx = i;
    *p = dst;
}

static int parse_picks(struct genre_s *g)
{
    size_t nm = 4;
    regmatch_t pm[nm];
    char buf[255];
    char *t = NULL;
    char *url = NULL, *desc = NULL, *home = NULL, *playing = NULL;
    int ret = 0;
    int min, max, bits, rank, page = 0, pages = 0;
    int i;
    char *p;
    struct pick_s this;

    p = g->urldata->data;

    if (regexec(&pick_page_r, p, nm, pm, 0) != REG_NOMATCH) {
	t = p;
	t += pm[1].rm_so;
	strncpy(buf, t, pm[1].rm_eo - pm[1].rm_so);
	buf[pm[1].rm_eo - pm[1].rm_so] = '\0';
	page = atoi(buf);
	t = p;
	t += pm[2].rm_so;
	strncpy(buf, t, pm[2].rm_eo - pm[2].rm_so);
	buf[pm[2].rm_eo - pm[2].rm_so] = '\0';
	pages = atoi(buf);
    }

    p = g->urldata->data;

  again:
    if (regexec(&pick_rank_r, p, nm, pm, 0) == REG_NOMATCH)
	goto done;

    if (pm[1].rm_so == -1)
	goto done;

    t = p;
    t += pm[1].rm_so;
    strncpy(buf, t, pm[1].rm_eo - pm[1].rm_so);
    buf[pm[1].rm_eo - pm[1].rm_so] = '\0';
    rank = atoi(buf);
    p += pm[1].rm_eo;

    /*
     * This is needed because the last page is always 20 picks no matter
     * what.
     */
    for (i = 0; i < g->pick; i++) {
	if (g->picks[i].rank == rank)
	    goto again;
    }

    if (regexec(&pick_url_r, p, nm, pm, 0) == REG_NOMATCH) {
	goto done;
    }

    if (pm[1].rm_so == -1)
	goto done;

    t = p;
    t += pm[1].rm_so;

    strncpy(buf, t, pm[1].rm_eo - pm[1].rm_so);
    buf[pm[1].rm_eo - pm[1].rm_so] = '\0';

    if ((url =
	 realloc(url,
		 (strlen(buf) + strlen(shoutcast_url) +
		  strlen("/sbin/shoutcast-playlist.pls?rn=&file=filename.pls")
		  + 1) * sizeof(char))) == NULL) {
	ret = 1;
	goto done;
    }

    sprintf(url, "%s/sbin/shoutcast-playlist.pls?rn=%s&file=filename.pls",
	    shoutcast_url, buf);
    p += pm[1].rm_eo;

    if (regexec(&pick_desc_r, p, nm, pm, 0) == REG_NOMATCH)
	goto done;

    if (pm[1].rm_so == -1 || pm[2].rm_so == -1)
	goto done;

    t = p;
    t += pm[1].rm_so;

    if ((home =
	 realloc(home,
		 (pm[1].rm_eo - pm[1].rm_so + 1) * sizeof(char))) == NULL) {
	ret = 1;
	goto done;
    }

    strncpy(home, t, pm[1].rm_eo - pm[1].rm_so);
    home[pm[1].rm_eo - pm[1].rm_so] = '\0';
    t = p;
    t += pm[2].rm_so;

    if ((desc =
	 realloc(desc,
		 (pm[2].rm_eo - pm[2].rm_so + 1) * sizeof(char))) == NULL) {
	ret = 1;
	goto done;
    }

    strncpy(desc, t, pm[2].rm_eo - pm[2].rm_so);
    desc[pm[2].rm_eo - pm[2].rm_so] = '\0';

    for (i = 0; i < strlen(desc); i++) {
	if (!isascii(desc[i]))
	    desc[i] = '!';
    }

    t = p;
    t += pm[3].rm_so + 2;
    p += pm[3].rm_eo;

    /*
     * No "Now playing: " (</font> not <br>). 
     */
    if (*t == 'f')
	goto skip_now_playing;

    if (regexec(&pick_play_r, p, nm, pm, 0) != REG_NOMATCH) {
	if (pm[1].rm_so == -1)
	    goto done;

	t = p;
	t += pm[1].rm_so;

	if ((playing =
	     realloc(playing,
		     (pm[1].rm_eo - pm[1].rm_so + 1) * sizeof(char))) ==
	    NULL) {
	    ret = 1;
	    goto done;
	}

	strncpy(playing, t, pm[1].rm_eo - pm[1].rm_so);
	playing[pm[1].rm_eo - pm[1].rm_so] = '\0';

	if (g->nodups) {
	    for (i = 0; i < g->pick; i++) {
		if (strcmp(g->picks[i].playing, playing) == 0)
		    goto again;
	    }
	}

	for (i = 0; i < strlen(playing); i++) {
	    if (!isascii(playing[i]))
		playing[i] = '!';
	}

	p += pm[1].rm_eo;
    }
    else
      skip_now_playing:
	playing = strdup(desc);

    if (regexec(&pick_user_r, p, nm, pm, 0) == REG_NOMATCH)
	goto done;

    if (pm[1].rm_so == -1 || pm[2].rm_so == -1)
	goto done;

    t = p;
    t += pm[1].rm_so;
    strncpy(buf, t, pm[1].rm_eo - pm[1].rm_so);
    buf[pm[1].rm_eo - pm[1].rm_so] = '\0';
    min = atoi(buf);
    t = p;
    t += pm[2].rm_so;
    strncpy(buf, t, pm[2].rm_eo - pm[2].rm_so);
    buf[pm[2].rm_eo - pm[2].rm_so] = '\0';
    max = atoi(buf);
    p += pm[2].rm_eo;

    if (regexec(&pick_bits_r, p, nm, pm, 0) == REG_NOMATCH)
	goto done;

    if (pm[1].rm_so == -1)
	goto done;

    t = p;
    t += pm[1].rm_so;
    strncpy(buf, t, pm[1].rm_eo - pm[1].rm_so);
    buf[pm[1].rm_eo - pm[1].rm_so] = '\0';
    bits = atoi(buf);

    p += pm[1].rm_eo;

    this.url = url;
    this.homepage = home;
    this.desc = desc;
    this.playing = playing;
    this.min = min;
    this.max = max;
    this.rank = rank;
    this.bitrate = bits;
    g->page = page;
    g->pages = pages;
    add_pick(&g->picks, &g->pick, this);
    goto again;

  done:
    if (url)
	free(url);

    if (desc)
	free(desc);

    if (home)
	free(home);

    if (playing)
	free(playing);

    return ret;
}

static void add_genre(unsigned int flags, char *title, char *url,
	int max_pages, int nodups)
{
    genres = realloc(genres, (ginfo.total + 1) * sizeof(struct genre_s));

    if (!genres) {
	se_message(0, 0, "%s: %i: realloc(): %s", __FILE__, __LINE__,
		   strerror(errno));
	return;
    }

    memset(&genres[ginfo.total], 0, sizeof(struct genre_s));
    genres[ginfo.total].url = url;
    genres[ginfo.total].title = title;

    if (!strcmp(title, "Search"))
	    flags |= GENRE_SEARCH;

    genres[ginfo.total].flags = flags;
    genres[ginfo.total].nodups = nodups;
    genres[ginfo.total].max_pages = max_pages;
    ginfo.total++;
}

static int parse_genres(char *data)
{
    size_t nm = 3;
    regmatch_t pm[nm];
    char *p = data;
    char buf[255];
    char *t;
    char str[255];
    unsigned flags = 0;
    char *title;

    free_genres_data();

    if (!data)
	return 1;

  again:
    if (regexec(&genre_r, p, nm, pm, 0) == REG_NOMATCH)
	goto done;

    if (pm[1].rm_so == -1 || pm[2].rm_so == -1)
	goto done;

    t = p;
    t += pm[1].rm_so;
    strncpy(buf, t, pm[1].rm_eo - pm[1].rm_so);
    buf[pm[1].rm_eo - pm[1].rm_so] = '\0';
    snprintf(str, sizeof(str), "%s/directory/?sgenre=%s", shoutcast_url, buf);
    t = p;
    t += pm[2].rm_so;
    strncpy(buf, t, pm[2].rm_eo - pm[2].rm_so);
    buf[pm[2].rm_eo - pm[2].rm_so] = '\0';
    flags |= GENRE_INIT;
    flags |= (buf[0] == ' ') ? 0 : GENRE_SECTION;
    title = strdup((buf[0] == ' ') ? buf + 3 : buf);

    if (config.collapse)
	flags |= GENRE_COLLAPSED;

    add_genre(flags, title, strdup(str), config.max_pages, config.nodups);
    p += pm[2].rm_eo;
    flags = 0;
    goto again;

  done:
    return 0;
}

static void update_genres_win()
{
    int i;
    int x = 0;
    int display = 0;
    static int maxx = -1, maxy = -1;

    if (maxx < ginfo.maxx || maxy < ginfo.maxy || !ginfo.maxy || !ginfo.maxx) {
	for (i = 0; i < ginfo.total; i++) {
	    if (strlen(genres[i].title) > x)
		x = strlen(genres[i].title) + 1;
	}

	if (x < strlen("[ Genres ]") + 1)
	    x = strlen("[ Genres ]") + 1;

	if (maxx < x)
	    maxx = ginfo.maxx = x;
	else
	    ginfo.maxx = x = maxx;

	if (maxy < LINES - 3)
	    maxy = ginfo.maxy = LINES - 3;
	else
	    ginfo.maxy = maxy;

	wresize(genre_w, LINES - 1, x + 2);
	wresize(pick_w, LINES - 1, COLS - (x + 2));
	move_panel(pick_p, 0, x + 2);
    }
    else {
	ginfo.maxx = maxx;
	ginfo.maxy = maxy;
    }

    for (i = 0; i <= ginfo.maxy; i++) {
	for (x = 0; x <= ginfo.maxx; x++)
	    mvwaddch(genre_w, i, x, ' ' | CP_GENRE_OTHER);
    }

    wclrtobot(genre_w);
    wattroff(genre_w, CP_GENRE_OTHER);

    if (ginfo.total <= ginfo.maxy || ginfo.y < ginfo.maxy / 2)
	i = 0;
    else
	i = (ginfo.y >
	     abs(ginfo.total - ginfo.maxy / 2)) ? ginfo.total -
	    ginfo.maxy : ginfo.y - ginfo.maxy / 2;

    for (x = 1; i < ginfo.total && x <= ginfo.maxy; i++) {
	int cp;
	int n = 0;

	if (i == ginfo.y)
	    cp = CP_GENRE_SELECTED;
	else if (genres[i].flags & GENRE_SECTION)
	    cp = CP_GENRE_SECTION;
	else if (genres[i].pick)
	    cp = CP_GENRE_PICKS;
	else
	    cp = CP_GENRE_OTHER;

	wattron(genre_w, cp);

	if (genres[i].flags & GENRE_SECTION) {
	    mvwprintw(genre_w, x, (i + 1 < ginfo.total && (genres[i + 1].flags & GENRE_SECTION)) ? 1 : 2, "%-*s", ginfo.maxx, genres[i].title);
	    n = 1;
	}
	else {
	    if (display) {
		genres[i].flags &= ~(GENRE_HIDDEN);
		mvwprintw(genre_w, x, 4, "%-*s", ginfo. maxx - 3, genres[i].title);
		n = 1;
	    }
	    else
		genres[i].flags |= GENRE_HIDDEN;
	}

	if ((genres[i].flags & GENRE_SECTION) && (genres[i].flags & GENRE_BOOKMARK)) {
	    wattroff(genre_w, cp);
	    mvwaddch(genre_w, x, ginfo.maxx, ACS_RARROW | CP_BOOKMARK);
	    wattron(genre_w, cp);
	}

	wattroff(genre_w, cp);

	if ((genres[i].flags & GENRE_SECTION) && i + 1 < ginfo.total &&
		!(genres[i + 1].flags & GENRE_SECTION)) {
	    if (genres[i].flags & GENRE_COLLAPSED) {
		display = 0;
		mvwaddch(genre_w, x, 1, '+' | CP_LINE_ART);
	    }
	    else {
		mvwaddch(genre_w, x, 1, '-' | CP_LINE_ART);
		display = 1;
	    }
	}

	if (!display) {
	    x += n;
	    continue;
	}

	if (!(genres[i].flags & GENRE_SECTION))
	    mvwaddch(genre_w, x, 1, ' ' | CP_GENRE_OTHER);

	if ((i + 1 < ginfo.total && (genres[i + 1].flags & GENRE_SECTION)
	     && i)
	    || (i == ginfo.total - 1 && !(genres[i].flags & GENRE_SECTION)))
	    mvwaddch(genre_w, x, 2, ACS_LLCORNER | CP_LINE_ART);
	else if (!(genres[i].flags & GENRE_SECTION) && i)
	    mvwaddch(genre_w, x, 2, ACS_LTEE | CP_LINE_ART);

	if (!(genres[i].flags & GENRE_SECTION))
	    mvwaddch(genre_w, x, 3, (genres[i].flags & GENRE_BOOKMARK) ?
		     ACS_RARROW | CP_BOOKMARK : ACS_HLINE | CP_LINE_ART);

	x++;
    }
}

static int update_genres(struct genre_s *g)
{
    int i;
    CURLMsg *cmessage = NULL;

    update_panels();
    doupdate();

    /*
     * Keep getting data until the whole page is received or until error. This
     * is only for genres. Picks are asyncronous.
     */
    wtimeout(genre_w, TIMEOUT);

    while (1) {
	chtype c = wgetch(genre_w);

	if (c == 'Q' || c == 'q' || c == '\033')
	    return 1;

	if ((cmerror =
	     curl_multi_perform(cmh, &nhandles)) == CURLM_CALL_MULTI_PERFORM
	    || nhandles > 0)
	    continue;
	else if (cmerror != CURLE_OK) {
	    if ((cmessage = curl_multi_info_read(cmh, &i)) == NULL)
		se_message(0, 0, "libCURL error:\n\nUnknown error");
	    else
		se_message(0, 0, "libCURL error:\n\n%s",
			   curl_easy_strerror(cmessage->data.result));

	    return 1;
	}

	break;
    }

    if (!g->urldata || parse_genres(g->urldata->data))
	return 1;

    free(g->urldata->data);
    free(g->urldata);

    /*
     * for (i = 0; i < genre; i++) {
     * genre_items = realloc(genre_items, (i + 2) * sizeof(ITEM));
     * genre_items[i] = new_item(genres[i].title, NULL);
     * 
     * if (genres[i].title[0] != ' ' && genres[i].title[0] != '-')
     * set_item_opts(genre_items[i], ~O_SELECTABLE);
     * }
     * 
     * if (genre)
     * genre_items[i] = NULL;
     */

    return (ginfo.total) ? 0 : 1;
}

int add_plugin(char *str)
{
    void *h;
    char *file;
    int i;
    chtype key;
    char *k;
    ncast_plugin_init *init;
    char *key_name = NULL;
    struct passwd *p;
    char buf[FILENAME_MAX];
    char *args = NULL;

    if ((file = strsep(&str, ":")) == NULL)
	return 1;

    if (!str || !str[0])
	return 1;

    if ((k = strsep(&str, ":")) == NULL)
	return 1;

    if (*k == 'F' || *k == 'f') {
	k++;

	i = atoi(k);
	key = KEY_F(i);
	key_name = k;
	k--;
    }
    else
	key = *k;

    for (i = 0; i < plugin; i++) {
	if (plugins[i].binding[0].key == key) {
	    se_message(0, 0, "'%s': key is already bound", k);
	    return 1;
	}
    }

    for (i = 0; i < sizeof(bindings) / sizeof(bindings[0]); i++) {
	if (bindings[i].key == key) {
	    se_message(0, 0, "'%s': key is already bound", k);
	    return 1;
	}
    }

    if (*file == '~') {
	p = getpwuid(getuid());
	snprintf(buf, sizeof(buf), "%s/%s", p->pw_dir, file + 2);
	file = buf;
    }
    else if (*file == '.' && file[1] == '/');
    else if (*file != '/') {
	p = getpwuid(getuid());
	snprintf(buf, sizeof(buf), "%s/.ncast/plugins/%s", p->pw_dir, file);
	file = buf;
    }

    if ((h = dlopen(file, RTLD_LAZY)) == NULL)
	errx(EXIT_FAILURE, "%s", dlerror());

    if ((init = dlsym(h, "ncast_plugin_init")) != NULL) {
	if (str != NULL)
	    (*init) (&args);
	else
	    str = (*init) (&args);
    }

    plugins = realloc(plugins, (plugin + 1) * sizeof(struct plugin_s));
    plugins[plugin].h = h;
    plugins[plugin].binding = calloc(2, sizeof(struct binding_s));
    plugins[plugin].binding[0].key = key;
    plugins[plugin].binding[0].keyname = key_name;
    plugins[plugin].args = args;

    if (str)
	plugins[plugin].binding[0].desc = strdup(str);

    plugins[plugin].binding[1].key = -1;
    plugin++;
    return 0;
}

static void clear_pick_win()
{
    int y, x;

    for (y = 0; y < LINES - 1; y++) {
	for (x = 0; x < COLS - (gcols + 2); x++)
	    mvwprintw(pick_w, y, x, " ");
    }
}

static void update_picks_win(struct pick_s *p, int total, int which)
{
    int i, x;

    if (!total)
	return;

    wmove(pick_w, 0, 0);
    wclrtobot(pick_w);

    if (total <= ginfo.maxy || which < ginfo.maxy / 2)
	i = 0;
    else
	i = (which > abs(total - ginfo.maxy / 2)) ? total - ginfo.maxy :
	    which - ginfo.maxy / 2;

    for (x = 1; i < total && x <= ginfo.maxy; i++, x++) {
	int n;
	char *s = (status.title == PLAYING) ? p[i].playing : p[i].desc;

	if (i == which)
	    wattron(pick_w, CP_PICK_SELECTED);
	else
	    wattron(pick_w, CP_PICK_OTHER);

	for (n = 0; s[n] && n < COLS - ginfo.maxx - 4; n++)
	    mvwaddch(pick_w, x, 1 + n, s[n]);

	while (n++ < COLS - ginfo.maxx - 4)
	    mvwaddch(pick_w, x, n, ' ');

	if (i == which)
	    wattroff(pick_w, CP_PICK_SELECTED);
	else
	    wattroff(pick_w, CP_PICK_OTHER);
    }
}

int send_to_player(const char *format, ...)
{
    va_list ap;
    int len;
    char *line;
    int try = 0;

    if (!player_initialized)
	return 0;

    va_start(ap, format);
#ifdef HAVE_VASPRINTF
    len = vasprintf(&line, format, ap);
#else
    if ((line = malloc(LINE_MAX)) == NULL)
	return -1;
    len = vsnprintf(line, LINE_MAX, format, ap);
#endif
    va_end(ap);

    while (1) {
	int n;
	fd_set fds;
	struct timeval tv;

	FD_ZERO(&fds);
	FD_SET(playerfd[1], &fds);

	tv.tv_sec = 0;
	tv.tv_usec = 0;

	if ((n = select(playerfd[1] + 1, NULL, &fds, NULL, &tv)) > 0) {
	    if (FD_ISSET(playerfd[1], &fds)) {
		n = write(playerfd[1], line, len);

		if (n == -1) {
		    if (errno == EAGAIN)
			continue;

		    if (kill(playerpid, 0) == -1) {
			se_message(0, 0, "Could not write to player. "
				   "Process no longer exists.");
			return -1;
		    }

		    se_message(0, 0, "Attempt #%i. write(): %s", ++try,
			       strerror(errno));
		    continue;
		}

		if (len != n) {
		    se_message(0, 0, "try #%i: write() error to player. "
			       "Expected %i, got %i.", ++try, len, n);
		    continue;
		}

		break;
	    }
	}
	else {
	    /*
	     * timeout 
	     */
	}
    }

    free(line);
    return 0;
}

#ifndef UNIX98
/* From the Xboard and screen packages. */
static int get_pty(char *pty_name)
{
    int i;
    int fd;

    for (i = 0; PTY_MAJOR[i]; i++) {
	int n;

	for (n = 0; PTY_MINOR[n]; n++) {
	    sprintf(pty_name, "%spty%c%c", _PATH_DEV, PTY_MAJOR[i],
		    PTY_MINOR[n]);

	    if ((fd = open(pty_name, O_RDWR | O_NOCTTY)) == -1) {
		se_message(0, 0, "%s: %s", pty_name, strerror(errno));
		continue;
	    }

	    sprintf(pty_name, "%stty%c%c", _PATH_DEV, PTY_MAJOR[i],
		    PTY_MINOR[n]);

	    if (access(pty_name, R_OK | W_OK) == -1) {
		close(fd);
		continue;
	    }

	    return fd;
	}
    }

    return -1;
}
#endif

/* Is this dangerous if pty permissions are wrong? */
static pid_t init_player(char **args)
{
    pid_t pid;
    int from[2], to[2];

#ifndef UNIX98
    char pty[255];

    if ((to[1] = get_pty(pty)) == -1) {
	errno = 0;
	return -1;
    }
#else

    if ((to[1] = open("/dev/ptmx", O_RDWR | O_NOCTTY)) == -1) {
	if (curses_initialized)
	    se_message(0, 0, "/dev/ptmx: %s", strerror(errno));

	return -1;
    }

    if (grantpt(to[1]) == -1) {
	if (curses_initialized)
	    se_message(0, 0, "grantpt(): %s: %s", ptsname(to[1]),
		       strerror(errno));

	return -1;
    }

    if (unlockpt(to[1]) == -1) {
	if (curses_initialized)
	    se_message(0, 0, "unlockpt(): %s: %s", ptsname(to[1]),
		       strerror(errno));

	return -1;
    }
#endif

    from[0] = to[1];
    errno = 0;

#ifndef UNIX98
    if ((to[0] = open(pty, O_RDWR | O_NOCTTY)) == -1) {
#else
    if ((to[0] = open(ptsname(to[1]), O_RDWR | O_NOCTTY)) == -1) {
#endif
	if (curses_initialized)
#ifndef UNIX98
	    se_message(0, 0, "%s: %s", pty, strerror(errno));
#else
	    se_message(0, 0, "%s: %s", ptsname(to[1]), strerror(errno));
#endif

	return -1;
    }

    from[1] = to[0];
    errno = 0;

    switch ((pid = fork())) {
	case -1:
	    return -2;
	case 0:
	    dup2(to[0], STDIN_FILENO);
	    dup2(from[1], STDOUT_FILENO);
	    close(to[0]);
	    close(to[1]);
	    close(from[0]);
	    close(from[1]);
	    dup2(STDOUT_FILENO, STDERR_FILENO);
	    execvp(args[0], args);
	    _exit(EXIT_FAILURE);
	default:
	    break;
    }

    if (kill(pid, 0) == -1)
	return -2;

    close(to[0]);
    close(from[1]);

    playerfd[0] = from[0];
    playerfd[1] = to[1];

    fcntl(playerfd[0], F_SETFL, O_NONBLOCK | O_DIRECT);
    fcntl(playerfd[1], F_SETFL, O_NONBLOCK | O_DIRECT);
    return pid;
}

static void sighandler(int sig)
{
    int s;

    switch (sig) {
	case SIGCHLD:
	    waitpid(-1, &s, WNOHANG);
	    break;
	default:
	    break;
    }
}

static void stop_player()
{
    send_to_player("quit\n");

    if (kill(playerpid, 0) != -1)
	kill(playerpid, SIGTERM);

    if (kill(playerpid, 0) != -1)
	kill(playerpid, SIGKILL);

    player_initialized = 0;
    return;
}

static int start_player(char **args)
{
    playerpid = init_player(args);

    switch (playerpid) {
	case -1:
	    /*
	     * Pty allocation. 
	     */
	    se_message(0, 0, "Could not allocate PTY");
	    break;
	case -2:
	    /*
	     * Could not execute engine. 
	     */
	    message(0, 0, "%s: %s", args[0], strerror(errno));
	    break;
	default:
	    //message(0, 0, "player fork()ed");
	    break;
    }

    player_initialized = 1;
    return playerpid;
}

/* 
 * parseargs.c
 *
 * This will parse a line used as an argument list for the exec() line of
 * functions returning a dynamically allocated array of character pointers so
 * you should free() it afterwards. Both ' and " quoting is supported (with
 * escapes) for multi-word arguments.
 *
 * This is my second attempt at it. Works alot better than the first. :)
 *
 * 2002/10/05
 * Ben Kibbey <bjk@luxsci.net>
 *
 */

static char **parseargs(char *str)
{
    char **pptr, *s;
    char arg[255];
    int idx = 0;
    int quote = 0;
    int lastchar = 0;
    int i;

    if (!str)
	return NULL;

    if (!(pptr = malloc(sizeof(char *))))
	return NULL;

    for (i = 0, s = str; *s; lastchar = *s++) {
	if ((*s == '\"' || *s == '\'') && lastchar != '\\') {
	    quote = (quote) ? 0 : 1;
	    continue;
	}

	if (*s == ' ' && !quote) {
	    arg[i] = 0;
	    pptr = realloc(pptr, (idx + 2) * sizeof(char *));
	    pptr[idx++] = strdup(arg);
	    arg[0] = i = 0;
	    continue;
	}

	if ((i + 1) == sizeof(arg))
	    continue;

	arg[i++] = *s;
    }

    arg[i] = 0;

    if (arg[0]) {
	pptr = realloc(pptr, (idx + 2) * sizeof(char *));
	pptr[idx++] = strdup(arg);
    }

    pptr[idx] = NULL;
    return pptr;
}

static void do_player(char *url)
{
    int i;
    int reset = 0;
    char **args = NULL;
    char *buf = NULL;
    char *cmd = "mpg123 -R";

    for (i = 0; i < plugin; i++) {
	if (plugins[i].touched) {
	    plugins[i].touched = 0;
	    reset = 1;
	}
    }

    if (reset || !player_initialized) {
	if (player_initialized)
	    stop_player();

	if ((buf =
	     realloc(buf,
		     (strlen(cmd)+1) * sizeof(char))) == NULL)
	    return;

	strcpy(buf, cmd);

	for (i = 0; i < plugin; i++) {
	    if (plugins[i].args) {
		if ((buf =
		     realloc(buf,
			     (strlen(buf) + strlen(plugins[i].args) +
			      2) * sizeof(char))) == NULL)
		    return;

		strcat(buf, " ");
		strcat(buf, plugins[i].args);
	    }
	}

	args = parseargs(buf);
    }

    if (!player_initialized)
	start_player(args);

    send_to_player("load %s\n", url);

    if (args) {
	for (i = 0; args[i]; i++)
	    free(args[i]);

	free(args);
	free(buf);
    }
}

static char *search_what(int which, struct pick_s p)
{
    if (which == SEARCH_HOMEPAGE)
	return p.homepage;
    else if (which == SEARCH_PLAYING)
	return p.playing;
    else if (which == SEARCH_DESC)
	return p.desc;

    return NULL;
}

static char *search_prompt(int which)
{
    if (which == SEARCH_HOMEPAGE)
	return "Homepage: ";
    else if (which == SEARCH_PLAYING)
	return "Playing: ";
    else if (which == SEARCH_DESC)
	return "Description: ";
    else if (which == SEARCH_SERVER)
	return "SHOUTcast: ";

    return NULL;
}

static void do_server_search(const char *str)
{
    char tmp[255];
    struct genre_s *g = &genres[ginfo.total-1];
    CURLMcode error;
    char *p;

    remove_handle(g, 1);

    snprintf(tmp, sizeof(tmp), "%s/directory/?s=%s", shoutcast_url, str);
    g->buf = realloc(g->buf, strlen(tmp)*sizeof(char)+1);
    sprintf(g->buf, "%s", tmp);

    for (p = g->buf; *p; p++) {
	if (*p == ' ')
	    *p = '+';
    }

    if (set_curl_easy_opts(g))
	return;

    if ((error = curl_multi_add_handle(cmh, g->ch)) != CURLE_OK) {
	se_message(0, 0, "%s", curl_multi_strerror(error));
	return;
    }
}

static int do_search_picks(struct search_s *sp)
{
    regex_t r;
    WINDOW *w = NULL;
    WINDOW *sw;
    PANEL *p = NULL;
    FIELD *fields[2];
    FORM *f = NULL;
    int i;
    static int last = -1;
    int ret;
    char errbuf[255];
    int did_regcomp = 0;
    int nomatch = 0;
    static char buf[80];
    int y, x;
    int item = sp->gp->y;
    int search_change = 0;
    int scaseflag = REG_ICASE;

    if (buf[0] != '\0') {
	for (i = 0; i < strlen(buf); i++) {
	    if (isupper(buf[i])) {
		scaseflag = 0;
		break;
	    }
	}
    }

    switch (sp->key) {
	case 0:
	case '/':
getpattern:
	    fields[0] = new_field(1, COLS - strlen(search_prompt(sp->which)),
		    0, 0, 0, 0);
	    set_field_buffer(fields[0], 0, buf);
	    set_field_opts(fields[0], ~(O_BLANK));
	    set_field_fore(fields[0], CP_SEARCH_WINDOW);
	    fields[1] = NULL;
	    w = newwin(1, COLS, LINES - 1, 0);
	    keypad(w, TRUE);
	    curs_set(1);
	    p = new_panel(w);
	    f = new_form(fields);
	    set_form_win(f, w);
	    scale_form(f, &y, &x);
	    sw = derwin(w, y, x, 0, strlen(search_prompt(sp->which)));
	    set_form_sub(f, sw);
	    post_form(f);
	    wbkgd(w, CP_SEARCH_WINDOW);
	    mvwprintw(w, 0, 0, search_prompt(sp->which));
	    form_driver(f, REQ_END_LINE);
	    update_panels();
	    doupdate();

	    while (1) {
		char *t;
		int c;

		BLOCKING(w);

		if ((c = wgetch(w)) == ERR) {
		    pth_yield(NULL);
		    continue;
		}

		switch (c) {
		    case '\n':
			strncpy(buf, field_buffer(fields[0], 0), sizeof(buf));
			t = buf + (strlen(buf) - 1);

			/*
			 * The field buffer is padded with spaces so we trim
			 * the trailing spaces. If a space is needed at the
			 * end of the expression, trailing with '[ ]' should
			 * fix it.
			 */
			while (*t == ' ')
			    *t-- = '\0';

			if (sp->which == SEARCH_SERVER) {
			    if (!buf[0])
				goto done;

			    if (strcmp(sp->gp->title, "Search")) {
				ginfo.y = ginfo.total - 1;

				if (strcmp(genres[ginfo.y].title, "Search"))
				    add_genre(GENRE_SECTION, strdup("Search"), NULL, config.max_pages, config.nodups);
				ginfo.y = ginfo.total - 1;
			    }

			    do_server_search(buf);
			    goto done;
			}

			if ((ret = regcomp(&r, buf, REG_EXTENDED|scaseflag|REG_NOSUB)) != 0) {
			    regerror(ret, &r, errbuf, sizeof(errbuf));
			    message(0, 0, "Error compiling expression: %s", errbuf);
			    break;
			}

			did_regcomp = 1;

			for (i = item; i < sp->gp->pick; i++) {
			    if (regexec(&r, search_what(sp->which, 
					    sp->gp->picks[i]), 0, 0, 0) != REG_NOMATCH) {
				if (i == last)
				    continue;

				sp->gp->y = item = i;
				goto done;
			    }
			}

			for (i = 0; i < sp->gp->pick; i++) {
			    if (regexec(&r, search_what(sp->which, 
					    sp->gp->picks[i]), 0, 0, 0) != REG_NOMATCH) {
				if (i == last)
				    continue;

				sp->gp->y = item = i;
				goto done;
			    }
			}

			nomatch = 1;
			goto done;
			break;
		    case '\033':
			goto done;
		    case KEY_BACKSPACE:
			form_driver(f, REQ_DEL_PREV);
			break;
		    case KEY_DC:
			form_driver(f, REQ_DEL_CHAR);
			break;
		    case KEY_RIGHT:
			form_driver(f, REQ_NEXT_CHAR);
			break;
		    case KEY_LEFT:
			form_driver(f, REQ_PREV_CHAR);
			break;
		    case KEY_HOME:
		    case CTRL('a'):
			form_driver(f, REQ_BEG_LINE);
			break;
		    case KEY_END:
		    case CTRL('e'):
			form_driver(f, REQ_END_LINE);
			break;
		    case CTRL('f'):
			form_driver(f, REQ_NEXT_WORD);
			break;
		    case CTRL('b'):
			form_driver(f, REQ_PREV_WORD);
			break;
		    case CTRL('k'):
			form_driver(f, REQ_CLR_EOL);
			break;
		    case CTRL('u'):
			form_driver(f, REQ_CLR_FIELD);
			break;
		    case KEY_DOWN:
			if (sp->which - 1 < 0)
			    sp->which = SEARCH_MAX - 1;
			else
			    sp->which--;
			search_change = 1;
			goto done;
			break;
		    case KEY_UP:
			if (sp->which + 1 == SEARCH_MAX)
			    sp->which = 0;
			else
			    sp->which++;
			search_change = 1;
			goto done;
			break;
		    default:
			form_driver(f, c);
			break;
		}

		wbkgd(w, CP_SEARCH_WINDOW);
		form_driver(f, REQ_VALIDATION);
		update_panels();
		doupdate();

		if (buf[0] != '\0') {
		    for (i = 0; i < strlen(buf); i++) {
			if (isupper(buf[i])) {
			    scaseflag = 0;
			    break;
			}
		    }
		}
	    }

done:
	    if (p)
		del_panel(p);

	    if (w)
		delwin(w);

	    if (f) {
		unpost_form(f);
		free_field(fields[0]);
		free_form(f);
	    }

	    if (did_regcomp)
		regfree(&r);

	    curs_set(0);
	    break;
	case 'n':
	case 'N':
	    if (!buf || !*buf)
		goto getpattern;

	    if ((ret = regcomp(&r, buf, REG_EXTENDED|scaseflag|REG_NOSUB)) != 0) {
		regerror(ret, &r, errbuf, sizeof(errbuf));
		message(0, 0, "Error compiling expression: %s", errbuf);
		break;
	    }

	    did_regcomp = 1;
	    i = (sp->key == 'n') ? last + 1 : last - 1;
	    i = (i < 0) ? sp->gp->pick - 1 : i;
	    i = (i > sp->gp->pick - 1) ? 0 : i;

	    for (; (sp->key == 'n') ? i < sp->gp->pick : i >= 0; (sp->key == 'n') ? i++ : i--) {
		if (regexec(&r, search_what(sp->which, sp->gp->picks[i]), 0, 
			    0, 0) != REG_NOMATCH) {
		    sp->gp->y = item = i;
		    goto done;
		}
	    }

	    for (i = (sp->key == 'n') ? 0 : sp->gp->pick - 1; (sp->key == 'n') ? i < sp->gp->pick : i >= 0; (sp->key == 'n') ? i++ : i--) {
		if (regexec(&r, search_what(sp->which, sp->gp->picks[i]), 0, 
			    0, 0) != REG_NOMATCH) {
		    sp->gp->y = item = i;
		    goto done;
		}
	    }
	    break;
	default:
	    break;
    }

    last = item;
    return (search_change) ? 1 : 0;
}

static void *search_thread_init(void *data)
{
    struct search_s *sp = data;

    while (do_search_picks(sp));
    return NULL;
}

static void search_picks(int which, struct genre_s *gp)
{
    pth_t th;

    if (!search) {
	if ((search = malloc(sizeof(struct search_s))) == NULL) {
	    se_message(0, 0, "%s: %i: malloc(): %s", __FILE__, __LINE__,
		    strerror(errno));
	    return;
	}

	search->which = !which ? SEARCH_SERVER : SEARCH_PLAYING;
    }
    else
	search->which = !which ? SEARCH_SERVER : SEARCH_PLAYING;

    search->key = which;
    search->gp = gp;

    if ((th = pth_spawn(PTH_ATTR_DEFAULT, search_thread_init, search)) == NULL)
	se_message(0, 0, "%s: %i: pth_spawn() failed", __FILE__, __LINE__);
    else
	pth_yield(th);
}

static int pick_desc_compare(const void *s1, const void *s2)
{
    const struct pick_s *p1 = s1;
    const struct pick_s *p2 = s2;

    return strcmp(p1->desc, p2->desc);
}

static int pick_rdesc_compare(const void *s1, const void *s2)
{
    const struct pick_s *p1 = s1;
    const struct pick_s *p2 = s2;
    int i = strcmp(p1->desc, p2->desc);

    if (i == 0)
	return 0;

    return (i > 0) ? -1 : 1;
}

static int pick_playing_compare(const void *s1, const void *s2)
{
    const struct pick_s *p1 = s1;
    const struct pick_s *p2 = s2;

    return strcmp(p1->playing, p2->playing);
}

static int pick_rplaying_compare(const void *s1, const void *s2)
{
    const struct pick_s *p1 = s1;
    const struct pick_s *p2 = s2;
    int i = strcmp(p1->playing, p2->playing);

    if (i == 0)
	return 0;

    return (i > 0) ? -1 : 1;
}

static int pick_bitrate_compare(const void *s1, const void *s2)
{
    const struct pick_s *p1 = s1;
    const struct pick_s *p2 = s2;

    if (p1->bitrate == p2->bitrate)
	return 0;

    return (p1->bitrate > p2->bitrate) ? 1 : -1;
}

static int pick_rbitrate_compare(const void *s1, const void *s2)
{
    const struct pick_s *p1 = s1;
    const struct pick_s *p2 = s2;

    if (p1->bitrate == p2->bitrate)
	return 0;

    return (p1->bitrate > p2->bitrate) ? -1 : 1;
}

static int pick_limit_compare(const void *s1, const void *s2)
{
    const struct pick_s *p1 = s1;
    const struct pick_s *p2 = s2;

    if (p1->max == p2->max)
	return 0;

    return (p1->max > p2->max) ? -1 : 1;
}

static int pick_rlimit_compare(const void *s1, const void *s2)
{
    const struct pick_s *p1 = s1;
    const struct pick_s *p2 = s2;

    if (p1->max == p2->max)
	return 0;

    return (p1->max < p2->max) ? -1 : 1;
}

static int pick_user_compare(const void *s1, const void *s2)
{
    const struct pick_s *p1 = s1;
    const struct pick_s *p2 = s2;

    if (p1->min == p2->min)
	return 0;

    return (p1->min > p2->min) ? -1 : 1;
}

static int pick_ruser_compare(const void *s1, const void *s2)
{
    const struct pick_s *p1 = s1;
    const struct pick_s *p2 = s2;

    if (p1->min == p2->min)
	return 0;

    return (p1->min < p2->min) ? -1 : 1;
}

static int pick_rank_compare(const void *s1, const void *s2)
{
    const struct pick_s *p1 = s1;
    const struct pick_s *p2 = s2;

    if (p1->rank == p2->rank)
	return 0;

    return (p1->rank < p2->rank) ? -1 : 1;
}

static int pick_rrank_compare(const void *s1, const void *s2)
{
    const struct pick_s *p1 = s1;
    const struct pick_s *p2 = s2;

    if (p1->rank == p2->rank)
	return 0;

    return (p1->rank > p2->rank) ? -1 : 1;
}

static int (*sort_which()) (const void *s1, const void *s2) {
    switch (status.sort) {
	case SORT_PLAYING:
	    if (status.reverse_sort == 0)
		return &pick_playing_compare;
	    return &pick_rplaying_compare;
	case SORT_BITS:
	    if (status.reverse_sort == 0)
		return &pick_bitrate_compare;
	    return &pick_rbitrate_compare;
	case SORT_DESC:
	    if (status.reverse_sort == 0)
		return &pick_desc_compare;
	    return &pick_rdesc_compare;
	case SORT_USER:
	    if (status.reverse_sort == 0)
		return &pick_user_compare;
	    return &pick_ruser_compare;
	case SORT_LIMIT:
	    if (status.reverse_sort == 0)
		return &pick_limit_compare;
	    return &pick_rlimit_compare;
	case SORT_RANK:
	    if (status.reverse_sort == 0)
		return &pick_rank_compare;
	    return &pick_rrank_compare;
	default:
	    break;
    }

    return NULL;
}

static char *addhelp(struct binding_s *b, char *str, int x)
{
    char *line = str;
    int len = (line) ? strlen(line) : 0;
    int i;

    for (i = 0; b[i].key != -1; i++) {
	char key[2];
	char buf[255] = { 0 };
	int n, p;

	if (b[i].desc == NULL)
	    continue;

	if (b[i].keyname == NULL) {
	    key[0] = b[i].key;
	    key[1] = '\0';
	    n = x - 1;
	}
	else
	    n = x - strlen(b[i].keyname);

	p = 0;

	while (n--)
	    buf[p++] = ' ';

	buf[p] = '\0';
	strcat(buf, (b[i].keyname) ? b[i].keyname : key);
	strcat(buf, " - ");
	strcat(buf, b[i].desc);
	strcat(buf, "\n");

	len += strlen(buf);

	if ((line = realloc(line, len + 1)) == NULL) {
	    se_message(0, 0, "%s: %i: realloc(): %s", __FILE__, __LINE__,
		       strerror(errno));
	    return NULL;
	}

	if (len - strlen(buf) == 0)
	    line[0] = '\0';

	strcat(line, buf);
    }

    return line;
}

static void help(struct binding_s *b)
{
    char *line = NULL;
    int i, x = 0;

    for (i = 0; bindings[i].key != -1; i++) {
	if (bindings[i].keyname && strlen(bindings[i].keyname) > x)
	    x = strlen(bindings[i].keyname);
    }

    for (i = 0; b[i].key != -1; i++) {
	if (b[i].keyname && strlen(b[i].keyname) > x)
	    x = strlen(b[i].keyname);
    }

    for (i = 0; i < plugin; i++) {
	if (plugins[i].binding[0].keyname
	    && strlen(plugins[i].binding[0].keyname) > x)
	    x = strlen(plugins[i].binding[0].keyname);
    }

    line = addhelp(bindings, line, x);
    line = addhelp(b, line, x);

    for (i = 0; i < plugin; i++)
	line = addhelp(plugins[i].binding, line, x);

    message(0, 0, "%s", line);
    free(line);
}

static void pick_info(char *g, struct pick_s pick)
{
    message(10, 1,
	    "   Genre: %s\n     URL: %s\nHomepage: %s\n Playing: %s\n    Info: %s\n    Rank: %i\n    Bits: %i\n   Users: %i/%i",
	    g, pick.url, pick.homepage, pick.playing, pick.desc, pick.rank,
	    pick.bitrate, pick.min, pick.max);
}

static void set_defaults()
{
    config.sort = SORT_RANK;
    config.max_pages = 5;
    config.colors = 1;
    set_default_colors();
}

static int do_bookmarks()
{
    FILE *fp;
    char *p;
    char buf[LINE_MAX];
    int i;

    if ((fp = fopen(bookfile, "r")) == NULL) {
	if (config.bmstartup || errno != ENOENT)
	    message(0, 0, "%s\n%s", bookfile, strerror(errno));

	return 1;
    }

    while ((p = fgets(buf, sizeof(buf), fp)) != NULL) {
	char *t;
	int which = -1;
	int n = -1;

	if ((t = strsep(&p, "\t")) != NULL) {
	    // Section or catagory.
	    for (i = 0; i < ginfo.total; i++) {
		if (strcmp(genres[i].title, t) == 0) {
		    which = i;
		    break;
		}
	    }

	    t = strsep(&p, "\t");

	    if (which >= 0) {
		// Subsection or subcatagory.
		for (i = which; i < ginfo.total; i++) {
		    if (strcmp(genres[i].title, t) == 0) {
			which = n = i;
			break;
		    }
		}
	    }

	    if (which == -1 && n == -1) {
		if ((genres =
		     realloc(genres,
			     (ginfo.total + 1) * sizeof(struct genre_s))) ==
		    NULL) {
		    se_message(0, 0, "%s: %i: realloc(): %s", __FILE__,
			       __LINE__, strerror(errno));
		    return 1;
		}

		which = ginfo.total++;
		memset(&genres[which], 0, sizeof(struct genre_s));
		genres[which].flags |= GENRE_INIT;
		genres[which].max_pages = config.max_pages;
		genres[which].nodups = config.nodups;
		genres[which].title = strdup(t);
	    }
	}

	if ((genres[which].bookmarks = realloc(genres[which].bookmarks,
					       (genres[which].bookmark +
						1) *
					       sizeof(struct pick_s))) ==
	    NULL) {
	    message(0, 0, "%s: %i: realloc(): %s", __FILE__, __LINE__,
		    strerror(errno));
	    return 1;
	}

	if ((t = strsep(&p, "\t")) != NULL)
	    genres[which].bookmarks[genres[which].bookmark].url = strdup(t);

	if ((t = strsep(&p, "\t")) != NULL)
	    genres[which].bookmarks[genres[which].bookmark].homepage =
		strdup(t);

	if ((t = strsep(&p, "\t")) != NULL)
	    genres[which].bookmarks[genres[which].bookmark].desc = strdup(t);

	if ((t = strsep(&p, "\t")) != NULL)
	    genres[which].bookmarks[genres[which].bookmark].playing =
		strdup(t);

	if ((t = strsep(&p, "\t")) != NULL)
	    genres[which].bookmarks[genres[which].bookmark].bitrate = atoi(t);

	if ((t = strsep(&p, "\t")) != NULL)
	    genres[which].bookmarks[genres[which].bookmark].rank = atoi(t);

	if ((t = strsep(&p, "\t")) != NULL)
	    genres[which].bookmarks[genres[which].bookmark].min = atoi(t);

	if ((t = strsep(&p, "\t")) != NULL)
	    genres[which].bookmarks[genres[which].bookmark].max = atoi(t);

	genres[which].flags |= GENRE_BOOKMARK;
	genres[which].bookmark++;
    }

    fclose(fp);
    status.state = BOOKMARKS;
    return 0;
}

static void init_regcomp()
{
    int ret;
    char errbuf[255];

    if ((ret =
	 regcomp(&genre_r, GENRE_REGEX, REG_EXTENDED | REG_NEWLINE)) != 0) {
	regerror(ret, &genre_r, errbuf, sizeof(errbuf));
	errx(EXIT_FAILURE, "GENRE_REGEX: %s", errbuf);
    }

    if ((ret = regcomp(&pick_rank_r, PICK_RANK_REGEX, REG_EXTENDED)) != 0) {
	regerror(ret, &genre_r, errbuf, sizeof(errbuf));
	errx(EXIT_FAILURE, "PICK_RANK_REGEX: %s", errbuf);
    }

    if ((ret = regcomp(&pick_url_r, PICK_URL_REGEX, REG_EXTENDED)) != 0) {
	regerror(ret, &genre_r, errbuf, sizeof(errbuf));
	errx(EXIT_FAILURE, "PICK_URL_REGEX: %s", errbuf);
    }

    if ((ret = regcomp(&pick_desc_r, PICK_DESC_REGEX, REG_EXTENDED)) != 0) {
	regerror(ret, &genre_r, errbuf, sizeof(errbuf));
	errx(EXIT_FAILURE, "PICK_DESC_REGEX: %s", errbuf);
    }

    if ((ret = regcomp(&pick_play_r, PICK_PLAY_REGEX, REG_EXTENDED)) != 0) {
	regerror(ret, &genre_r, errbuf, sizeof(errbuf));
	errx(EXIT_FAILURE, "PICK_PLAY_REGEX: %s", errbuf);
    }

    if ((ret = regcomp(&pick_page_r, PICK_PAGE_REGEX, REG_EXTENDED)) != 0) {
	regerror(ret, &genre_r, errbuf, sizeof(errbuf));
	errx(EXIT_FAILURE, "PICK_PAGE_REGEX: %s", errbuf);
    }

    if ((ret = regcomp(&pick_bits_r, PICK_BITS_REGEX, REG_EXTENDED)) != 0) {
	regerror(ret, &genre_r, errbuf, sizeof(errbuf));
	errx(EXIT_FAILURE, "PICK_BITS_REGEX: %s", errbuf);
    }

    if ((ret = regcomp(&pick_user_r, PICK_USER_REGEX, REG_EXTENDED)) != 0) {
	regerror(ret, &genre_r, errbuf, sizeof(errbuf));
	errx(EXIT_FAILURE, "PICK_USER_REGEX: %s", errbuf);
    }
}

static void show_error(int n)
{
    if (curses_initialized)
	se_message(0, 0, "Error:\n\n%s", strerror(n));
    else
	errx(EXIT_FAILURE, "%s", strerror(n));
}

size_t curl_write(void *ptr, size_t size, size_t nmemb, void *data)
{
    size_t len = size * nmemb;
    struct urldata_s *d = data;

    if (len == 0)
	return len;

    if ((d->data = realloc(d->data, d->size + len + 1)) != NULL) {
	memcpy(&(d->data[d->size]), ptr, len);
	d->size += len;
	d->data[d->size] = '\0';
    }

    return len;
}

size_t curl_header(void *ptr, size_t size, size_t nmemb, void *data)
{
    size_t len = size * nmemb;
    char *p;

    if (len == 0)
	return len;

    if (strncmp(ptr, "Set-Cookie: ", 12) == 0) {
	p = ptr;
	p += 12;
	p[strlen(p) - 10] = '\0';

	if ((cookie =
	     realloc(cookie, (strlen(p) + 1) * sizeof(char))) == NULL) {
	    se_message(0, 0, "%s: %i: realloc(): %s", __FILE__, __LINE__,
		       strerror(errno));
	    return -1;
	}

	strncpy(cookie, p, strlen(p));
    }

    return len;
}

static int _set_curl_opts(CURL **h, struct urldata_s **urldata,
	const char *url)
{
    CURL *ch = *h;
    int error;
    struct urldata_s *data = *urldata;

    if (!ch) {
	if ((ch = curl_easy_init()) == NULL) {
	    if (curses_initialized) {
		se_message(0, 0, "libCURL Error:\ncurl_easy_init() failed");
		return 1;
	    }
	    else
		errx(EXIT_FAILURE, "curl_easy_init() failed");
	}
    }
    else
	curl_easy_reset(ch);

    if ((error = curl_easy_setopt(ch, CURLOPT_FAILONERROR, 1)) != CURLE_OK) {
	se_message(0, 0, "%s", curl_easy_strerror(error));
	return 1;
    }

    if ((error =
	 curl_easy_setopt(ch, CURLOPT_WRITEFUNCTION,
			  curl_write)) != CURLE_OK) {
	se_message(0, 0, "%s", curl_easy_strerror(error));
	return 1;
    }

    if ((error =
	 curl_easy_setopt(ch, CURLOPT_HEADERFUNCTION,
			  curl_header)) != CURLE_OK) {
	se_message(0, 0, "%s", curl_easy_strerror(error));
	return 1;
    }

    if ((error =
	 curl_easy_setopt(ch, CURLOPT_URL, url) != CURLE_OK)) {
	se_message(0, 0, "%s", curl_easy_strerror(error));
	return 1;
    }

    if (cookie) {
	if ((error =
	     curl_easy_setopt(ch, CURLOPT_COOKIE, cookie)) != CURLE_OK) {
	    se_message(0, 0, "%s", curl_easy_strerror(error));
	    return 1;
	}
    }

    /*
     * This needs to be set for some reason. Cookies won't work without it.
     * * Could be data corruption, but I can't find it. 
     */
    if ((error = curl_easy_setopt(ch, CURLOPT_HEADER, 1)) != CURLE_OK) {
	se_message(0, 0, "%s", curl_easy_strerror(error));
	return 1;
    }

    if ((error = curl_easy_setopt(ch, CURLOPT_ENCODING, "")) != CURLE_OK) {
	se_message(0, 0, "%s", curl_easy_strerror(error));
	return 1;
    }

    if (!data) {
	if ((data = calloc(1, sizeof(struct urldata_s))) == NULL) {
	    show_error(errno);
	    return 1;
	}
    }

    data->size = 0;

    if ((error =
	 curl_easy_setopt(ch, CURLOPT_WRITEDATA, data)) != CURLE_OK) {
	se_message(0, 0, "%s", curl_easy_strerror(error));
	return 1;
    }

    if ((error = curl_easy_setopt(ch, CURLOPT_HEADERDATA, data)) != CURLE_OK) {
	se_message(0, 0, "%s", curl_easy_strerror(error));
	return 1;
    }

    *h = ch;
    *urldata = data;
    return 0;
}

int set_curl_easy_opts(struct genre_s *g)
{
    return _set_curl_opts(&g->ch, &g->urldata, g->buf ? g->buf : g->url);
}

static int do_picks(struct genre_s *g)
{
    int error;

    if (set_curl_easy_opts(g))
	return 1;

    if ((error = curl_multi_add_handle(cmh, g->ch)) != CURLE_OK) {
	se_message(0, 0, "%s", curl_multi_strerror(error));
	return 1;
    }

    clear_pick_win();
    g->flags &= ~(GENRE_INIT);
    g->flags &= ~(GENRE_DONE);
    g->flags |= GENRE_CONNECT;
    return 0;
}

static int init_curl(struct genre_s **ginit)
{
    struct genre_s *g = *ginit;

    if (curl_global_init(CURL_GLOBAL_ALL) != 0) {
	se_message(0, 0, "%s: %i: curl_global_init() failed", __FILE__,
		   __LINE__);
	return 1;
    }

    if ((cmh = curl_multi_init()) == NULL) {
	se_message(0, 0, "%s: %i: curl_multi_init() failed", __FILE__,
		   __LINE__);
	return 1;
    }

    if ((g = calloc(1, sizeof(struct genre_s))) == NULL) {
	se_message(0, 0, "%s: %i: calloc() %s", __FILE__, __LINE__,
		   strerror(errno));
	return 1;
    }

    g->url = shoutcast_url;

    if (set_curl_easy_opts(g) == 0) {
	if ((cmerror = curl_multi_add_handle(cmh, g->ch)) != CURLE_OK) {
	    se_message(0, 0, "%s", curl_easy_strerror(cmerror));
	    return 1;
	}
    }

    *ginit = g;
    curl_initialized = 1;
    return 0;
}

static void read_genre_cache()
{
    char buf[FILENAME_MAX];
    FILE *fp;
    struct genre_rw_s *gp;
    int len = 0;
    int n;

    snprintf(buf, sizeof(buf), "%s/.ncast/genres.cache", pw->pw_dir);

    if ((fp = fopen(buf, "r")) == NULL) {
	if (errno != ENOENT)
	    message(0, 0, "%s\n%s", buf, strerror(errno));

	return;
    }

    if ((n = fread(&len, 1, sizeof(int), fp)) != sizeof(int)) {
	message(0, 0, "fread(): read %i, wanted %i", n, sizeof(int));
	fclose(fp);
	return;
    }

    if ((n = fread(&ginfo.y, 1, sizeof(int), fp)) != sizeof(int)) {
	message(0, 0, "fread(): read %i, wanted %i", n, sizeof(int));
	fclose(fp);
	return;
    }

    if ((gp = malloc(sizeof(struct genre_rw_s) + len)) == NULL) {
	message(0, 0, "%s: %i: malloc(): %s", __FILE__, __LINE__,
		strerror(errno));
	fclose(fp);
	return;
    }

    while (1) {
	char *t, *p;

	if ((n = fread(gp, 1, sizeof(struct genre_rw_s) + len, fp)) !=
		sizeof(struct genre_rw_s) + len) {
	    if (!feof(fp)) {
		message(0, 0, "fread(): wanted %i got %i",
			sizeof(struct genre_rw_s) + len, n);
		continue;
	    }
	}

	if (feof(fp))
	    break;

	p = gp->buf;

	if ((t = strsep(&p, "\t")) != NULL) {
	    char *url = strdup(t);
	    char *title = strdup(strsep(&p, "\t"));

	    add_genre(gp->flags, title, url, gp->max_pages, gp->nodups);
	}
	else
	    message(0, 0, "genres cache file parse error");
    }

    fclose(fp);
    free(gp);
}

static int do_genres(struct genre_s *g)
{
    int s = status.state;
    static int did_cachefile;
    int i;

    if (!did_cachefile) {
	read_genre_cache();
	did_cachefile = 1;
    }

    if (!ginfo.total) {
	status.state = FETCH_GENRES;
	update_status(NULL);

	if (update_genres(g)) {
	    message(0, 0, "Could not find any genres on host");
	    add_genre(GENRE_SECTION, strdup("Search"), NULL, config.max_pages, config.nodups);
	    status.state = s;
	    return 1;
	}
    }

    if (strcmp(genres[ginfo.total-1].title, "Search"))
	add_genre(GENRE_SECTION, strdup("Search"), NULL, config.max_pages, config.nodups);

    do_bookmarks();
    update_genres_win();
    status.state = DONE;
    focus = GENRE;

    for (i = 0; i < ginfo.total; i++) {
	if (genres[i].flags & GENRE_AUTOFETCH)
	    do_picks(&genres[i]);
    }

    return 0;
}

static void draw_window_decor(struct genre_s *g)
{
    if (focus == PICK) {
	wattron(genre_w, CP_INACTIVE_BORDER);
	box(genre_w, ACS_VLINE, ACS_HLINE);
	mvwprintw(genre_w, 0, 1, "[ Genres ]");
	wattroff(genre_w, CP_INACTIVE_BORDER);
	wattron(pick_w, CP_ACTIVE_BORDER);
	box(pick_w, ACS_VLINE, ACS_HLINE);
	mvwprintw(pick_w, 0, 1, "[ %s - sorting by %s%s ]",
		  (status.title == PLAYING) ? (status.state == BOOKMARKS) ? "Bookmarks" : "Picks" : (status.state == BOOKMARKS) ? "Bookmark Server" : "Pick Server", sort_strings[status.sort], (status.reverse_sort) ? " (reverse)" : "");

	if (g && g->pick && g->bookmark)
	    mvwprintw(pick_w, ginfo.maxy + 1,
		    COLS - ginfo.maxx - 4 - 2, "[%c]", ACS_RARROW);

	wattroff(pick_w, CP_ACTIVE_BORDER);
	show_panel(pick_p);
    }
    else {
	wattron(genre_w, CP_ACTIVE_BORDER);
	box(genre_w, ACS_VLINE, ACS_HLINE);
	mvwprintw(genre_w, 0, 1, "[ Genres ]");
	wattroff(genre_w, CP_ACTIVE_BORDER);
	wattron(pick_w, CP_INACTIVE_BORDER);
	box(pick_w, ACS_VLINE, ACS_HLINE);
	mvwprintw(pick_w, 0, 1, "[ %s - sorting by %s%s ]",
		  (status.title == PLAYING) ? (status.state == BOOKMARKS || (g && g->bookmark && !g->pick)) ? "Bookmarks" : "Picks" : (status.state == BOOKMARKS) ? "Bookmark Server" : "Pick Server", sort_strings[status.sort], (status.reverse_sort) ? " (reverse)" : "");

	if (g && g->pick && g->bookmark)
	    mvwprintw(pick_w, ginfo.maxy + 1,
		    COLS - ginfo.maxx - 4 - 2, "[%c]", ACS_RARROW);

	wattroff(pick_w, CP_INACTIVE_BORDER);
    }

    if (ginfo.total && ginfo.total > ginfo.maxy) {
	if (ginfo.total && ginfo.y - ginfo.maxy / 2 > 0)
	    mvwaddch(genre_w, 1, ginfo.maxx + 1, ACS_UARROW | CP_SCROLL_ARROW);

	if (ginfo.total && ginfo.total - ginfo.maxy / 2 > ginfo.y)
	    mvwaddch(genre_w, ginfo.maxy, ginfo.maxx + 1,
		    ACS_DARROW | CP_SCROLL_ARROW);
    }

    if (g && g->pick && g->pick > ginfo.maxy) {
	if (g && g->pick && g->y - ginfo.maxy / 2 > 0)
	    mvwaddch(pick_w, 1, COLS - ginfo.maxx - 3,
		    ACS_UARROW | CP_SCROLL_ARROW);

	if (g && g->pick && g->pick - ginfo.maxy / 2 > g->y)
	    mvwaddch(pick_w, ginfo.maxy, COLS - ginfo.maxx - 3,
		    ACS_DARROW | CP_SCROLL_ARROW);
    }
}

static void remove_handle(struct genre_s *g, int clean)
{
    int error;

    if (!g->ch)
	return;

    if (g->flags & GENRE_SEARCH || g->flags & GENRE_CONNECT ||
	    !(g->flags & GENRE_DONE) || g->flags & GENRE_REFRESH) {

	if ((error = curl_multi_remove_handle(cmh, g->ch)) != CURLE_OK &&
	    error != CURLM_BAD_EASY_HANDLE)
	    se_message(0, 0, "%s: %i: %s", __FILE__, __LINE__,
		       curl_multi_strerror(error));
	else {
	    g->ch = NULL;
	    g->flags &= ~(GENRE_FETCH) | ~(GENRE_CONNECT);
	    g->flags |= GENRE_DONE;

	    if (g->buf)
		free(g->buf);

	    g->buf = NULL;

	    if (clean) {
		free_picks(g->picks, g->pick);
		g->pick = g->y = 0;
		g->page = g->pages = 0;
	    }

	    if ((g->urldata->data =
		 realloc(g->urldata->data, 1 * sizeof(char))) == NULL)
		se_message(0, 0, "realloc(): %s", strerror(errno));

	    g->urldata->size = 0;
	}
    }
}

static void save_genres()
{
    int i;
    int mlen = 0;
    FILE *fp;
    char buf[FILENAME_MAX];
    struct genre_rw_s *g;

    snprintf(buf, sizeof(buf), "%s/.ncast/genres.cache", pw->pw_dir);

    if ((fp = fopen(buf, "w+")) == NULL) {
	message(0, 0, "%s\n%s", buf, strerror(errno));
	return;
    }

    for (i = 0; i < ginfo.total; i++) {
	if (genres[i].url && strlen(genres[i].url) + strlen(genres[i].title) > mlen)
	    mlen = strlen(genres[i].url) + strlen(genres[i].title);
	else if (strlen("UNUSED") + strlen(genres[i].title) > mlen)
	    mlen = strlen("UNUSED") + strlen(genres[i].title);
    }

    mlen += 2;

    if ((i = fwrite(&mlen, 1, sizeof(int), fp)) != sizeof(int)) {
	message(0, 0, "fwrite(): wrote %i, wanted %i", i, sizeof(int));
	fclose(fp);
	return;
    }

    if ((i = fwrite(&ginfo.y, 1, sizeof(int), fp)) != sizeof(int)) {
	message(0, 0, "fwrite(): wrote %i, wanted %i", i, sizeof(int));
	fclose(fp);
	return;
    }

    if ((g = malloc(sizeof(struct genre_rw_s) + mlen)) == NULL) {
	message(0, 0, "%s: %i: malloc()", __FILE__, __LINE__, strerror(errno));
	fclose(fp);
	return;
    }

    for (i = 0; i < ginfo.total; i++) {
	int n;

	memset(&g, sizeof(struct genre_rw_s) + mlen, 0);
	g->flags = genres[i].flags;
	g->max_pages = genres[i].max_pages;
	g->nodups = genres[i].nodups;
	snprintf(g->buf, mlen, "%s\t%s", genres[i].url ? genres[i].url : "UNUSED", genres[i].title);

	if ((n = fwrite(g, 1, sizeof(struct genre_rw_s) + mlen, fp)) != 
		sizeof(struct genre_rw_s) + mlen) {
	    message(0, 0, "fwrite(): wrote %i, wanted %i", n, sizeof(struct genre_rw_s) + mlen);
	    fclose(fp);
	    free(g);
	    return;
	}
    }

    fclose(fp);
    free(g);
}

static void save_bookmarks()
{
    int i;
    FILE *fp;
    char buf[FILENAME_MAX];

    if (!ginfo.total)
	return;

    for (i = 0; i < ginfo.total; i++) {
	if (genres[i].flags & GENRE_BOOKMARK) {
	    i = -1;
	    break;
	}
    }

    snprintf(buf, sizeof(buf), "%s/.ncast/bookmarks", pw->pw_dir);

    if (i != -1) {
	unlink(buf);
	return;
    }

    if ((fp = fopen(buf, "w+")) == NULL) {
	message(0, 0, "%s:\n%s", buf, strerror(errno));
	return;
    }

    for (i = 0; i < ginfo.total; i++) {
	int n;
	int x = i;

	if (genres[i].flags & GENRE_BOOKMARK) {
	    if (!(genres[i].flags & GENRE_SECTION)) {
		for (x = i; x >= 0; x--) {
		    if (genres[x].flags & GENRE_SECTION)
			break;

		    if (x - 1 == -1)
			x--;
		}
	    }

	    for (n = 0; n < genres[i].bookmark; n++)
		fprintf(fp, "%s\t%s\t%s\t%s\t%s\t%s\t%i\t%i\t%i\t%i\n",
			(x >= 0) ? genres[x].title : genres[i].title,
			genres[i].title,
			genres[i].bookmarks[n].url,
			genres[i].bookmarks[n].homepage,
			genres[i].bookmarks[n].desc,
			genres[i].bookmarks[n].playing,
			genres[i].bookmarks[n].bitrate,
			genres[i].bookmarks[n].rank,
			genres[i].bookmarks[n].min,
			genres[i].bookmarks[n].max);
	}
    }

    fclose(fp);
}

static void remove_bookmark()
{
    struct pick_s *new = NULL;
    int i;
    int n = 0;

    for (i = 0; i < genres[ginfo.y].bookmark; i++) {
	if (i == genres[ginfo.y].by) {
	    free(genres[ginfo.y].bookmarks[i].playing);
	    free(genres[ginfo.y].bookmarks[i].desc);
	    free(genres[ginfo.y].bookmarks[i].homepage);
	    free(genres[ginfo.y].bookmarks[i].url);
	    continue;
	}

	if ((new = realloc(new, (n + 1) * sizeof(struct pick_s))) == NULL) {
	    message(0, 0, "%s: %i: realloc(): %s", __FILE__, __LINE__,
		    strerror(errno));
	    return;
	}

	new[n++] = genres[ginfo.y].bookmarks[i];
    }

    free(genres[ginfo.y].bookmarks); // ??
    genres[ginfo.y].bookmarks = new;
    genres[ginfo.y].bookmark = n;

    if (genres[ginfo.y].by >= genres[ginfo.y].bookmark - 1)
	genres[ginfo.y].by = genres[ginfo.y].bookmark - 1;

    if (!genres[ginfo.y].bookmark) {
	genres[ginfo.y].flags &= ~(GENRE_BOOKMARK);
	genres[ginfo.y].by = 0;
	update_genres_win();
    }
}

static void delete_genre()
{
    int i;

    if (strcmp(genres[ginfo.y].title, "Search") == 0)
	return;

    if (genres[ginfo.y].flags & GENRE_SECTION) {
	while (1) {
	    if (ginfo.total-1 == 0)
		break;

	    free_genre_data_once(&genres[ginfo.y]);

	    for (i = ginfo.y; i < ginfo.total; i++)
		memcpy(&genres[i], &genres[i+1], sizeof(struct genre_s));

	    genres = realloc(genres, ginfo.total-- * sizeof(struct genre_s));

	    if (genres[ginfo.y].flags & GENRE_SECTION || ginfo.y >= ginfo.total)
		break;
	}
    }
    else {
	free_genre_data_once(&genres[ginfo.y]);

	for (i = ginfo.y; i < ginfo.total; i++)
	    memcpy(&genres[i], &genres[i+1], sizeof(struct genre_s));

	genres = realloc(genres, ginfo.total * sizeof(struct genre_s));
	ginfo.total--;
    }

    if (ginfo.y >= ginfo.total)
	ginfo.y = ginfo.total - 1;

    update_genres_win();
}

static void fetch_playlist(struct genre_s *gp)
{
    if (_set_curl_opts(&gp->pch, &gp->purldata,
		status.state == BOOKMARKS ? gp->bookmarks[gp->by].url :
		gp->picks[gp->y].url))
	return;

    cmerror = curl_multi_add_handle(cmh, gp->pch);
    
    if (cmerror != CURLE_OK)
	se_message(0, 0, "%s", curl_multi_strerror (cmerror));
}

static void parse_playlist(struct genre_s *gp)
{
    char *p = gp->purldata->data;
    char *t;
    char url[LINE_MAX];

    p = strstr(gp->purldata->data, "File1=");

    if (!p)
	return;

    for (t = url; *p; p++) {
	if (*p == '\n')
	    break;

	*t++ = *p;
    }

    *t = 0;
    t = url;

    while (*t && *t != '=')
	t++;

    t++;

    for (p = url; *t; t++)
	*p++ = *t;

    *p = 0;
    do_player(url);
}

static void *progloop(void *data)
{
    ncast_plugin *ncp = NULL;
    struct genre_s *genre_init = NULL;
    int volume = 100;

    initscr();

    if (config.colors) {
	if (has_colors() == TRUE) {
	    if (start_color() != ERR) {
		use_default_colors();
		init_color_pairs();
	    }
	}
    }

    cmerror = CURLE_OK;
    curses_initialized = 1;
    curs_set(0);
    noecho();
    status_w = newwin(1, COLS, LINES - 1, 0);
    status_p = new_panel(status_w);
    genre_w = newwin(LINES - 1, 20, 0, 0);
    genre_p = new_panel(genre_w);
    pick_w = newwin(LINES - 1, COLS - 20, 0, 20);
    pick_p = new_panel(pick_w);
    keypad(genre_w, TRUE);
    keypad(pick_w, TRUE);
    focus = GENRE;
    status.title = PLAYING;

    draw_window_decor(NULL);

    if (!config.bmstartup) {
	if (!curl_initialized) {
	    if (init_curl(&genre_init)) {
		if (do_bookmarks())
		    goto done;
	    }
	}

	if (do_genres(genre_init))
	    do_bookmarks();
    }
    else {
	if (do_bookmarks()) {
	    if (!curl_initialized) {
		if (init_curl(&genre_init))
		    goto done;

		do_genres(genre_init);
	    }
	}
    }

    while (1) {
	int c;
	int i;
	WINDOW *wp = (focus == PICK) ? pick_w : genre_w;
	char tmp[255];
	CURLMsg *cmsg;
	struct timeval tv;
	struct genre_s *gp = &genres[ginfo.y];
	fd_set fdr, fdw, fde;
	int maxfd;

	if (curl_initialized) {
	    do {
		cmerror = curl_multi_perform(cmh, &nhandles);
	    } while (cmerror == CURLM_CALL_MULTI_PERFORM);

	    FD_ZERO(&fdr);
	    FD_ZERO(&fdw);
	    FD_ZERO(&fde);

	    if ((i =
		 curl_multi_fdset(cmh, &fdr, &fdw, &fde, &maxfd)) != CURLE_OK)
		message(0, 0, "curl_multi_fdset(): %s",
			curl_multi_strerror(i));

	    if (maxfd != -1) {
		wtimeout(wp, TIMEOUT);
		tv.tv_sec = tv.tv_usec = 0;

		switch (select(maxfd + 1, &fdr, &fdw, &fde, &tv)) {
		    case -1:
			message(0, 0, "select(): %s", strerror(errno));
			break;
		    case 0:
			break;
		    default:
			cmerror = curl_multi_perform(cmh, &nhandles);
			break;
		}
	    }
	    else
		wtimeout(wp, TIMEOUT);

	    while ((cmsg = curl_multi_info_read(cmh, &c)) != NULL) {
		if (cmsg->msg == CURLMSG_DONE) {
		    for (i = 0; i < ginfo.total; i++) {
			if (genres[i].ch == cmsg->easy_handle) {
			    genres[i].flags &= ~(GENRE_CONNECT);

			    if (cmsg->data.result != CURLE_OK) {
				se_message(0, 0, "%s: %s", genres[i].title,
					   curl_easy_strerror(cmsg->data.
							      result));
				break;
			    }

			    if (!(genres[i].flags & GENRE_DONE)) {
				parse_picks(&genres[i]);
				genres[i].urldata->size = 0;

				if ((!genres[i].max_pages
				     && genres[i].page < genres[i].pages)
				    || (genres[i].max_pages
					&& genres[i].page <
					genres[i].max_pages
					&& genres[i].page <
					genres[i].pages)) {
				    snprintf(tmp, sizeof(tmp),
					     "%s/index.phtml?startat=%i&sgenre=%s",
					     shoutcast_url,
					     genres[i].page * 20, genres[i].title);

				    if ((genres[i].buf =
					 realloc(genres[i].buf,
						 strlen(tmp) * sizeof(char) +
						 1)) == NULL) {
					se_message(0, 0, "realloc(): %s",
						   strerror(errno));
					break;
				    }

				    /*
				     * curl_easy_setopt(3) says CURLOPT_URL must
				     * * remain for the life of the handle because
				     * * it won't copy it. So...
				     */
				    strcpy(genres[i].buf, tmp);
				    curl_multi_remove_handle(cmh, genres[i].ch);
				    genres[i].ch = NULL;

				    if (set_curl_easy_opts(&genres[i]) == 0) {
					if ((cmerror = curl_multi_add_handle(cmh, genres[i].ch)) != CURLE_OK)
					    se_message(0, 0, "%s",
						       curl_multi_strerror
						       (cmerror));
				    }

				    qsort(genres[i].picks, genres[i].pick,
					    sizeof(struct pick_s), *(sort_which()));
				}
				else {
				    remove_handle(&genres[i], 0);
				    //update_picks_win(gp->picks, gp->pick, gp->y);
				}
			    }
			    else {
				parse_picks(&genres[i]);
				genres[i].urldata->size = 0;
			    }

			    break;
			}
			else if (genres[i].pch == cmsg->easy_handle) {
			    curl_multi_remove_handle(cmh, genres[i].pch);
			    genres[i].pch = NULL;

			    if (cmsg->data.result != CURLE_OK) {
				se_message(0, 0, "%s: %s", genres[i].title,
					   curl_easy_strerror(cmsg->data.
							      result));
				break;
			    }

			    parse_playlist(&genres[i]);
			}
		    }
		}
	    }

	    if (cmerror != CURLM_CALL_MULTI_PERFORM && cmerror != CURLE_OK)
		se_message(0, 0, "%i: %s", cmerror,
			   curl_multi_strerror(cmerror));
	    else
		update_genres_win();
	}

	if (ginfo.total && !gp->pick)
	    clear_pick_win();

	if (status.state == BOOKMARKS || (gp->bookmark && !gp->pick))
	    update_picks_win(gp->bookmarks, gp->bookmark, gp->by);
	else
	    update_picks_win(gp->picks, gp->pick, gp->y);

	if ((gp->flags & GENRE_INIT) && !(gp->flags & GENRE_BOOKMARK))
	    mvwaddstr(pick_w, 1, 1, "Press F1 for help.      ");
	else if ((gp->pick == 0 && gp->flags & GENRE_DONE) &&
		!(gp->flags & GENRE_BOOKMARK))
	    mvwaddstr(pick_w, 1, 1, "No picks for this genre.");

	if (pth_ctrl(PTH_CTRL_GETTHREADS_READY) > 0) {
	    bottom_panel(pick_p);
	    bottom_panel(genre_p);
	}

	/*
	 * Temporary fix for "flashing" dialog boxes. The panel is "ontop"
	 * before update_panels(), but not after. When threading is removed
	 * and a window stack is used instead, this will be fixed.
	 */
	if (!msg_box) {
	    draw_window_decor(gp);
	    update_status(gp);
	    update_panels();
	    doupdate();
	}

	if (pth_ctrl(PTH_CTRL_GETTHREADS_READY) > 0) {
	    pth_yield(NULL);
	    continue;
	}

	if ((c = wgetch(wp)) == ERR)
	    continue;

again:
	switch (focus) {
	    case GENRE:
		switch (c) {
		    case KEY_DC:
			delete_genre();
			break;
		    case ' ':
			if (!(gp->flags & GENRE_SECTION))
			    break;

			if (gp->flags & GENRE_COLLAPSED)
			    gp->flags &= ~(GENRE_COLLAPSED);
			else
			    gp->flags |= GENRE_COLLAPSED;

			update_genres_win();
			continue;
			break;
		    case KEY_UP:
			if (ginfo.y - 1 < 0)
			    ginfo.y = ginfo.total - 1;
			else {
			    ginfo.y--;

			    if (genres[ginfo.y].flags & GENRE_HIDDEN)
				goto again;
			}

			update_genres_win();
			break;
		    case KEY_DOWN:
			if (ginfo.y + 1 == ginfo.total)
			    ginfo.y = 0;
			else {
			    ginfo.y++;

			    if (genres[ginfo.y].flags & GENRE_HIDDEN)
				goto again;
			}

			update_genres_win();
			break;
		    case KEY_HOME:
			ginfo.y = 0;

			while (ginfo.y < ginfo.total && genres[ginfo.y].flags & GENRE_HIDDEN)
			    ginfo.y++;

			update_genres_win();
			break;
		    case KEY_END:
			ginfo.y = ginfo.total - 1;

			while (ginfo.y && genres[ginfo.y].flags & GENRE_HIDDEN)
			    ginfo.y--;

			update_genres_win();
			break;
		    case KEY_NPAGE:
			if (ginfo.total <= ginfo.maxy) {
			    ginfo.y = ginfo.total - 1;
			    break;
			}

			if (ginfo.y + ginfo.maxy / 2 >= ginfo.total)
			    ginfo.y = ginfo.total - 1;
			else
			    ginfo.y += ginfo.maxy / 2;

			update_genres_win();
			break;
		    case KEY_PPAGE:
			if (ginfo.total <= ginfo.maxy) {
			    ginfo.y = 0;
			    break;
			}

			if (ginfo.y - ginfo.maxy / 2 < 0)
			    ginfo.y = 0;
			else
			    ginfo.y -= ginfo.maxy / 2;

			update_genres_win();
			break;
		    case KEY_RIGHT:
			if (!(gp->flags & GENRE_BOOKMARK) || !gp->bookmark)
			    break;

			focus = PICK;
			status.state = BOOKMARKS;
			break;
		    default:
			break;
		}

		break;
	    case PICK:
		if (status.state != BOOKMARKS) {
		    switch (c) {
			case KEY_UP:
			    if (gp->y - 1 < 0)
				gp->y = gp->pick - 1;
			    else
				gp->y--;

			    break;
			case KEY_DOWN:
			    if (gp->y + 1 == gp->pick)
				gp->y = 0;
			    else
				gp->y++;

			    break;
			case KEY_HOME:
			    gp->y = 0;
			    break;
			case KEY_END:
			    gp->y = gp->pick - 1;
			    break;
			case KEY_NPAGE:
			    if (gp->pick <= ginfo.maxy) {
				gp->y = gp->pick - 1;
				break;
			    }

			    if (gp->y + ginfo.maxy / 2 >= gp->pick)
				gp->y = gp->pick - 1;
			    else
				gp->y += ginfo.maxy / 2;

			    break;
			case KEY_PPAGE:
			    if (gp->pick <= ginfo.maxy) {
				gp->y = 0;
				break;
			    }

			    if (gp->y - ginfo.maxy / 2 < 0)
				gp->y = 0;
			    else
				gp->y -= ginfo.maxy / 2;

			    break;
			case KEY_RIGHT:
			    if (gp->flags & GENRE_BOOKMARK)
				status.state = BOOKMARKS;
			    break;
			case KEY_LEFT:
			    focus = GENRE;
			    break;
			default:
			    break;
		    }
		}
		else {
		    switch (c) {
			case KEY_LEFT:
			    focus = GENRE;
			    status.state = DONE;
			    break;
			case KEY_RIGHT:
			    if (gp->pick)
				status.state = DONE;
			    break;
			case KEY_UP:
			    if (gp->by - 1 < 0)
				gp->by = gp->bookmark - 1;
			    else
				gp->by--;

			    break;
			case KEY_DOWN:
			    if (gp->by + 1 == gp->bookmark)
				gp->by = 0;
			    else
				gp->by++;

			    break;
			case KEY_HOME:
			    gp->by = 0;
			    break;
			case KEY_END:
			    gp->by = gp->bookmark - 1;
			    break;
			case KEY_NPAGE:
			    if (gp->bookmark <= ginfo.maxy) {
				gp->by = gp->bookmark - 1;
				break;
			    }

			    if (gp->by + ginfo.maxy / 2 >= gp->bookmark)
				gp->by = gp->bookmark - 1;
			    else
				gp->by += ginfo.maxy / 2;

			    break;
			case KEY_PPAGE:
			    if (gp->bookmark <= ginfo.maxy) {
				gp->by = 0;
				break;
			    }

			    if (gp->by - ginfo.maxy / 2 < 0)
				gp->by = 0;
			    else
				gp->by -= ginfo.maxy / 2;

			    break;
			default:
			    break;
		    }
		}
	    default:
		break;
	}

	switch (c) {
	    case '?':
		if (status.state == BOOKMARKS) {
		    if (!gp->bookmark)
			break;

		    pick_info(gp->title, gp->bookmarks[gp->by]);
		    break;
		}

		if (gp->pick == 0)
		    break;

		pick_info(gp->title, gp->picks[gp->y]);
		break;
	    case 'K':
		remove_handle(gp, 0);
		break;
	    case 'A':
		if (!strcmp(gp->title, "Search") || gp->flags & GENRE_AUTOFETCH)
		    gp->flags &= ~(GENRE_AUTOFETCH);
		else
		    gp->flags |= GENRE_AUTOFETCH;

		break;
	    case 'b':
		if (focus != PICK || status.state == BOOKMARKS)
		    break;

		add_pick(&gp->bookmarks, &gp->bookmark, gp->picks[gp->y]);
		gp->flags |= GENRE_BOOKMARK;
		update_genres_win();
		break;
	    case 'r':
		if (focus != PICK || (focus == PICK && status.state != BOOKMARKS))
		    break;

		remove_bookmark();

		if (!gp->bookmark) {
		    status.state = DONE;
		    focus = GENRE;
		}
		break;
	    case '=':
		if (status.title == PLAYING)
		    status.title = DESCRIPTION;
		else
		    status.title = PLAYING;
		break;
	    case '\n':
		if (focus == PICK) {
		    fetch_playlist(gp);
		    break;
		}

		if (strcmp(gp->title, "Search") == 0) {
		    if (search)
			search->which = SEARCH_SERVER;

		    search_picks(0, gp);
		    break;
		}

		if (ginfo.total && gp->pick) {
		    focus = PICK;
		    break;
		}

		if (ginfo.total) {
		    if (!curl_initialized) {
			if (init_curl(&genre_init))
			    break;
		    }

		    if (do_picks(gp) != 0)
			break;
		}
		break;
	    case 'R':
		if (focus == GENRE) {
		    free_genres_data();
		    init_curl(&genre_init);
		    do_genres(genre_init);
		    ginfo.y = 0;
		    update_genres_win();
		    break;
		}

		if (status.state == BOOKMARKS)
		    break;

		gp->flags |= GENRE_REFRESH;
		remove_handle(gp, 1);

		if (do_picks(gp) != 0)
		    break;
		break;
	    case '\t':
		if (focus == GENRE) {
		    if (!gp->pick && !gp->bookmark)
			break;

		    if (gp->bookmark && !gp->pick) {
			focus = PICK;
			status.state = BOOKMARKS;
			break;
		    }
		}

		focus = (focus == GENRE) ? PICK : GENRE;
		
		if (focus == GENRE && status.state == BOOKMARKS)
		    status.state = DONE;

		break;
	    case 'n':
	    case 'N':
	    case '/':
		search_picks(c, gp);
		break;
	    case 'O':
		status.reverse_sort = (status.reverse_sort) ? 0 : 1;

		if (status.state == BOOKMARKS)
		    qsort(gp->bookmarks, gp->bookmark, sizeof(struct pick_s),
			    *(sort_which()));
		else
		    qsort(gp->picks, gp->pick, sizeof(struct pick_s),
			    *(sort_which()));
		break;
	    case 'o':
		status.sort++;

		if (status.sort >= SORT_MAX)
		    status.sort = 0;

		if (status.state == BOOKMARKS)
		    qsort(gp->bookmarks, gp->bookmark, sizeof(struct pick_s),
			    *(sort_which()));
		else
		    qsort(gp->picks, gp->pick, sizeof(struct pick_s),
			    *(sort_which()));
		break;
	    case KEY_F(1):
		help(mainbindings);
		break;
	    case 'f':
		gp->nodups++;

		if (gp->nodups > 1)
		    gp->nodups = 0;

		break;
	    case ']':
		gp->max_pages++;
		break;
	    case '[':
		if (gp->max_pages - 1 < 0)
		    break;

		gp->max_pages--;
		break;
	    case 'Q':
		goto done;
	    case '-':
		volume -= 2;

		if (volume < 0)
		    volume = 0;

		send_to_player("volume %i\n", volume);
		break;
	    case '+':
		volume += 2;
		send_to_player("volume %i\n", volume);
		break;
	    default:
		for (i = 0; i < plugin; i++) {
		    if (c == plugins[i].binding[0].key) {
			if ((ncp =
			     dlsym(plugins[i].h, "ncast_plugin")) == NULL)
			    break;

			(*ncp)(send_to_player);
			update_panels();
			doupdate();
			break;
		    }
		}
		break;
	}
    }

  done:
    if (player_initialized)
	stop_player();

    save_bookmarks();
    save_genres();
    cleanup();

    curl_multi_cleanup(cmh);
    curl_global_cleanup();
    endwin();
    return NULL;
}

int main(int argc, char *argv[])
{
    int opt;
    char buf[FILENAME_MAX];
    struct stat st;
    int bm = 0;
    pth_once_t once_th = PTH_ONCE_INIT;

    shoutcast_url = SHOUTCAST_URL;

    if ((pw = getpwuid(getuid())) == NULL)
	errx(EXIT_FAILURE, "getpwuid() failed");

    snprintf(buf, sizeof(buf), "%s/.ncast", pw->pw_dir);
    snprintf(bookfile, sizeof(bookfile), "%s/bookmarks", buf);

    if (stat(buf, &st) == -1) {
	if (errno == ENOENT) {
	    if (mkdir(buf, 0755) == -1)
		se_message(0, 0, "%s: %s", buf, strerror(errno));
	    else
		message(0, 0, "%s created, put a configuration file in there",
			buf);
	}
	else
	    err(EXIT_FAILURE, "%s", buf);
    }

    while ((opt = getopt(argc, argv, "vhu:p:f:b")) != -1) {
	switch (opt) {
	    case 'b':
		bm = 1;
		break;
	    case 'f':
		strncpy(buf, optarg, sizeof(buf));
		break;
	    case 'p':
		if (add_plugin(optarg))
		    usage(argv[0], stderr);
		break;
	    case 'u':
		shoutcast_url = optarg;
		break;
	    case 'v':
		printf("%s\nNCurses is version %s (%i)\n", curl_version(),
		       NCURSES_VERSION, NCURSES_VERSION_PATCH);
		printf("%s written by %s.\n", PACKAGE_STRING,
		       PACKAGE_BUGREPORT);
		exit(EXIT_SUCCESS);
	    case 'h':
	    default:
		usage(argv[0], (opt == 'h') ? stdout : stderr);
	}
    }

    set_defaults();
    snprintf(buf, sizeof(buf), "%s/.ncast/config", pw->pw_dir);
    parse_rcfile(buf);
    status.sort = config.sort;
    status.reverse_sort = config.reverse_sort;
    signal(SIGCHLD, sighandler);

    if (bm)
	config.bmstartup = 1;

    if (pth_init() == FALSE)
	errx(EXIT_FAILURE, "pth_init() failed");

    threads_initialized = 1;
    init_regcomp();
    pth_once(&once_th, (void *) progloop, NULL);
    pth_yield(NULL);
    exit(EXIT_SUCCESS);
}
