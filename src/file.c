/* vim:tw=78:ts=8:sw=4:set ft=c:  */
/*
    Copyright (C) 2005 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_LIMITS_H
#include <limits.h>
#endif

#ifdef HAVE_NCURSES_H
#include <ncurses.h>
#endif

#include "common.h"
#include "colors.h"
#include "file.h"

static int attributes(const char *filename, int line, char *str)
{
    char *tmp;
    int attrs = 0;

    while ((tmp = strsep(&str, ",")) != NULL) {
	if (strcasecmp(tmp, "BOLD") == 0)
	    attrs |= A_BOLD;
	else if (strcasecmp(tmp, "REVERSE") == 0)
	    attrs |= A_REVERSE;
	else if (strcasecmp(tmp, "NONE") == 0)
	    attrs |= A_NORMAL;
	else if (strcasecmp(tmp, "DIM") == 0)
	    attrs |= A_DIM;
	else if (strcasecmp(tmp, "STANDOUT") == 0)
	    attrs |= A_STANDOUT;
	else if (strcasecmp(tmp, "UNDERLINE") == 0)
	    attrs |= A_UNDERLINE;
	else if (strcasecmp(tmp, "BLINK") == 0)
	    attrs |= A_BLINK;
	else if (strcasecmp(tmp, "INVISIBLE") == 0)
	    attrs |= A_INVIS;
	else
	    errx(EXIT_FAILURE, "%s(%i): invalid attribute \"%s\"", filename,
		 line, tmp);
    }

    return attrs;
}

static short color_name(const char *filename, int line, const char *color)
{
    if (strcasecmp(color, "BLACK") == 0)
	return COLOR_BLACK;
    else if (strcasecmp(color, "WHITE") == 0)
	return COLOR_WHITE;
    else if (strcasecmp(color, "GREEN") == 0)
	return COLOR_GREEN;
    else if (strcasecmp(color, "YELLOW") == 0)
	return COLOR_YELLOW;
    else if (strcasecmp(color, "MAGENTA") == 0)
	return COLOR_MAGENTA;
    else if (strcasecmp(color, "BLUE") == 0)
	return COLOR_BLUE;
    else if (strcasecmp(color, "RED") == 0)
	return COLOR_RED;
    else if (strcasecmp(color, "CYAN") == 0)
	return COLOR_CYAN;
    else
	errx(EXIT_FAILURE, "%s(%i): invalid color \"%s\"", filename, line,
	     color);

    return -1;
}

static void parse_color(const char *filename, int line, const char *str,
			struct color_s *c)
{
    char fg[16], bg[16], attr[64], nattr[64];
    struct color_s ctmp = *c;
    int n;

    if ((n = sscanf(str, "%[a-zA-Z] %[a-zA-Z] %[a-zA-Z,] %[a-zA-Z,]", fg, bg,
		    attr, nattr)) < 2)
	errx(EXIT_FAILURE, "%s(%i): parse error", filename, line);

    ctmp.fg = color_name(filename, line, fg);
    ctmp.bg = color_name(filename, line, bg);
    ctmp.attrs = ctmp.nattrs = 0;

    if (n > 2)
	ctmp.attrs = attributes(filename, line, attr);

    if (n > 3)
	ctmp.nattrs = attributes(filename, line, nattr);

    *c = ctmp;
    return;
}

static int isinteger(char *s)
{
    int i;

    for (i = 0; i < strlen(s); i++) {
	if (!isdigit(s[i]))
	    return 1;
    }

    return 0;
}

static char *trim(char *str)
{
    int i = 0;

    if (!str)
	return NULL;

    while (isspace(*str))
	str++;

    for (i = strlen(str) - 1; isspace(str[i]); i--)
	str[i] = 0;

    return str;
}

void parse_rcfile(const char *filename)
{
    FILE *fp;
    char *line, buf[LINE_MAX];
    int lines = 0;
    char *p, *t;

    if ((fp = fopen(filename, "r")) == NULL) {
	warn("%s", filename);
	warnx("Using defaults ...");
	return;
    }

    while ((line = fgets(buf, sizeof(buf), fp)) != NULL) {
	int n;
	char var[30], val[255];

	lines++;
	line = trim(line);

	if (!line[0] || line[0] == '#')
	    continue;

	if ((n = sscanf(line, "%s %[^\n]", var, val)) != 2)
	    errx(EXIT_FAILURE, "%s(%i): parse error %i", filename, lines, n);

	strncpy(val, trim(val), sizeof(val));
	strncpy(var, trim(var), sizeof(var));

	if (strcmp(var, "sort") == 0) {
	    if (strcmp(val, "play") == 0)
		config.sort = SORT_PLAYING;
	    else if (strcmp(val, "play_r") == 0) {
		config.sort = SORT_PLAYING;
		config.reverse_sort = 1;
	    }
	    else if (strcmp(val, "bitrate") == 0)
		config.sort = SORT_BITS;
	    else if (strcmp(val, "bitrate_r") == 0) {
		config.sort = SORT_BITS;
		config.reverse_sort = 1;
	    }
	    else if (strcmp(val, "info") == 0)
		config.sort = SORT_DESC;
	    else if (strcmp(val, "info_r") == 0) {
		config.sort = SORT_DESC;
		config.reverse_sort = 1;
	    }
	    else if (strcmp(val, "users") == 0)
		config.sort = SORT_USER;
	    else if (strcmp(val, "users_r") == 0) {
		config.sort = SORT_USER;
		config.reverse_sort = 1;
	    }
	    else if (strcmp(val, "limit") == 0)
		config.sort = SORT_LIMIT;
	    else if (strcmp(val, "limit_r") == 0) {
		config.sort = SORT_LIMIT;
		config.reverse_sort = 1;
	    }
	    else if (strcmp(val, "rank") == 0)
		config.sort = SORT_RANK;
	    else if (strcmp(val, "rank_r") == 0) {
		config.sort = SORT_RANK;
		config.reverse_sort = 1;
	    }
	    else
		errx(EXIT_FAILURE, "%s(%i): invalid sort method \"%s\"",
		     filename, lines, val);
	}
	else if (strcmp(var, "max_pages") == 0) {
	    if (isinteger(val))
		errx(EXIT_FAILURE, "%s(%i): value is not an integer",
		     filename, lines);

	    config.max_pages = atoi(val);
	}
	else if (strcmp(var, "bookmarks") == 0) {
	    if (isinteger(val))
		errx(EXIT_FAILURE, "%s(%i): value is not an integer",
		     filename, lines);

	    config.bmstartup = atoi(val);
	}
	else if (strcmp(var, "nodups") == 0) {
	    if (isinteger(val))
		errx(EXIT_FAILURE, "%s(%i): value is not an integer",
		     filename, lines);

	    config.nodups = atoi(val);

	    if (config.nodups < 0 || config.nodups > 1)
		errx(EXIT_FAILURE, "%s(%i): invalid value", filename, lines);
	}
	else if (strcmp(var, "plugin") == 0) {
	    t = val;

	    if ((p = strsep(&t, " \t")) == NULL)
		errx(EXIT_FAILURE, "plugin parse error");

	    strncpy(buf, p, sizeof(buf));

	    while (*t && isspace(*t))
		t++;

	    if ((p = strsep(&t, " \t")) == NULL)
		errx(EXIT_FAILURE, "plugin parse error");

	    strncat(buf, ":", sizeof(buf));
	    strncat(buf, p, sizeof(buf));

	    if (t != NULL) {
		while (*t && isspace(*t))
		    t++;

		strncat(buf, ":", sizeof(buf));
		strncat(buf, t, sizeof(buf));
	    }

	    if (add_plugin(buf))
		exit(EXIT_FAILURE);
	}
	else if (strcmp(var, "colors") == 0) {
	    if (isinteger(val))
		errx(EXIT_FAILURE, "%s(%i): value is not an integer",
		     filename, lines);

	    config.colors = atoi(val);

	    if (config.colors < 0 || config.colors > 1)
		errx(EXIT_FAILURE, "%s(%i): invalid value", filename, lines);
	}
	else if (strcmp(var, "collapse") == 0) {
	    if (isinteger(val))
		errx(EXIT_FAILURE, "%s(%i): value is not an integer",
		     filename, lines);

	    config.collapse = atoi(val);

	    if (config.collapse < 0 || config.collapse > 1)
		errx(EXIT_FAILURE, "%s(%i): invalid value", filename, lines);
	}
	else if (strcmp(var, "genre_selected") == 0)
	    parse_color(filename, lines, val, &config.color[0]);
	else if (strcmp(var, "genre_other") == 0)
	    parse_color(filename, lines, val, &config.color[1]);
	else if (strcmp(var, "genre_section") == 0)
	    parse_color(filename, lines, val, &config.color[2]);
	else if (strcmp(var, "pick_selected") == 0)
	    parse_color(filename, lines, val, &config.color[3]);
	else if (strcmp(var, "pick_other") == 0)
	    parse_color(filename, lines, val, &config.color[4]);
	else if (strcmp(var, "active_border") == 0)
	    parse_color(filename, lines, val, &config.color[5]);
	else if (strcmp(var, "inactive_border") == 0)
	    parse_color(filename, lines, val, &config.color[6]);
	else if (strcmp(var, "search_window") == 0)
	    parse_color(filename, lines, val, &config.color[7]);
	else if (strcmp(var, "status_window") == 0)
	    parse_color(filename, lines, val, &config.color[8]);
	else if (strcmp(var, "help_window") == 0)
	    parse_color(filename, lines, val, &config.color[9]);
	else if (strcmp(var, "help_border") == 0)
	    parse_color(filename, lines, val, &config.color[10]);
	else if (strcmp(var, "scroll_arrow") == 0)
	    parse_color(filename, lines, val, &config.color[11]);
	else if (strcmp(var, "bookmark_arrow") == 0)
	    parse_color(filename, lines, val, &config.color[12]);
	else if (strcmp(var, "line_art") == 0)
	    parse_color(filename, lines, val, &config.color[13]);
	else if (strcmp(var, "progress_bar") == 0)
	    parse_color(filename, lines, val, &config.color[14]);
	else if (strcmp(var, "genre_picks") == 0)
	    parse_color(filename, lines, val, &config.color[15]);
	else
	    errx(EXIT_FAILURE, "%s(%i): invalid parameter \"%s\"", filename,
		 lines, var);
    }

    fclose(fp);
}
