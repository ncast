/* vim:tw=78:ts=8:sw=4:set ft=c:  */
/*
    Copyright (C) 2005 Ben Kibbey <bjk@luxsci.net>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#ifndef ncast_H
#define ncast_H

/* Extended Regular Expressions found in the urldata structure that
 * CURL stores via curl_write(). See parse_picks().
 */
#define GENRE_REGEX	"<OPTION VALUE=[\"]?([^\">]*)[\"]?>([ -]?[^[:cntrl:]]*)"
#define PICK_RANK_REGEX	"<td[^>]*><font[^>]*><b>([0-9]*)</b>"
#define PICK_URL_REGEX	"/sbin/shoutcast-playlist.pls\\?rn\\=([0-9]*)"
#define PICK_DESC_REGEX	"scurl\" href=\"([^\"]*)\">([^<]*)</a>[[:space:]]*(</font>|<br>)"
#define PICK_PLAY_REGEX	"Now Playing:</font>[ ]*([^<]*)"
#define PICK_PAGE_REGEX	"Page ([0-9]*) of ([0-9]*)"
#define PICK_USER_REGEX ">([0-9]*)/([0-9]*)</font>"
#define PICK_BITS_REGEX ">([0-9]*)</font>"

/* 
 * Milliseconds until wgetch() quits waiting for input. Blocking will be used
 * if there is nothing to do remotely.
 */
#define TIMEOUT		80
#define BLOCKING(w)	(cmerror == CURLM_CALL_MULTI_PERFORM || nhandles > 0) ? wtimeout(w, 0) : wtimeout(w, -1)
#define CTRL(x)		((x) & 0x1f)
#define so_message(...)	_message(STDOUT_FILENO, __VA_ARGS__)
#define se_message(...)	_message(STDERR_FILENO, __VA_ARGS__)
#define message(...)	so_message(__VA_ARGS__)

CURLMcode cmerror;
static int nhandles;		/* Number of CURL handles receiving data. */
static char *shoutcast_url;
static char *cookie;		/* A single cookie used throughout the session. */
static regex_t genre_r;
static regex_t pick_rank_r;
static regex_t pick_url_r;
static regex_t pick_desc_r;
static regex_t pick_play_r;
static regex_t pick_page_r;
static regex_t pick_user_r;
static regex_t pick_bits_r;
static WINDOW *genre_w;
static PANEL *genre_p;
static WINDOW *pick_w;
static PANEL *pick_p;
static WINDOW *status_w;
static PANEL *status_p;
static pid_t playerpid;
static int playerfd[2];
static char bookfile[FILENAME_MAX];
static int curses_initialized;
static int player_initialized;
static int curl_initialized;
static int threads_initialized;
static int gcols;		/* Genre window columns. For resizing
				 * the pick window when switching
				 * between bookmarks/genres. */
struct passwd *pw;

static int focus;
enum {
    GENRE, PICK, PLAYING, DESCRIPTION
};

static char eqfile[PATH_MAX];
typedef int (*ncast_player_t)(const char *fmt, ...);
struct plugin_s *plugins;
static int plugin;		/* Total plugins. */
typedef void(ncast_plugin) (ncast_player_t);	/* When 'key' is pressed. */
typedef char *(ncast_plugin_init) (char **args);	/* Upon startup. */
typedef void (ncast_plugin_exit) (void);	/* Upon exit. */

/* Built-in keys. */
static struct binding_s {
    chtype key;
    char *keyname;
    char *desc;
} bindings[] = {
    { KEY_UP, "Up Arrow", "Previous menu entry"},
    { KEY_DOWN, "Down Arrow", "Next menu entry"},
    { KEY_END, "End", "Last menu entry"},
    { KEY_HOME, "Home", "First menu entry"},
    { KEY_NPAGE, "Page Down", "Next menu page"},
    { KEY_PPAGE, "Page Up", "Previous menu page"},
    { ' ', "Space", "Collapse/Uncollapse genre tree"},
    { KEY_DC, "Delete", "Remove selected genre"},
    { KEY_RIGHT, "Right Arrow", "Show bookmarks for this genre"},
    { '\n', "Enter", "Select menu entry"},
    { '\t', "Tab", "Toggle window focus"},
    { '/', NULL, "Search the pick window/New pattern"},
    { 'n', NULL, "Next pattern match"},
    { 'N', NULL, "Previous pattern match"},
    { 'o', NULL, "Cycle sorting method of pick window"},
    { 'O', NULL, "Toggle reverse sorting"},
    { '?', NULL, "Pick information"},
    { '=', NULL, "Toggle pick Playing/Description"},
    { KEY_F(1), NULL, NULL},
    { 'Q', NULL, "Quit"},
    { -1, NULL, NULL}
};

static struct binding_s mainbindings[] = {
    {'R', NULL, "Refresh genres or picks for the selected genre"},
    {'A', NULL, "Toggle auto fetching for this genre"},
    {'b', NULL, "Add a bookmark for this pick"},
    {'r', NULL, "Remove selected bookmark"},
    {'K', NULL, "Kill/stop fetching picks for this genre"},
    {']', "]/[", "Increment/deincrement fetched pages"},
    {'+', "+/-", "Increase/Decrease volume"},
    {'-', NULL, NULL},
    {'f', NULL, "Cycle filtering method of picks"},
    {-1, NULL, NULL}
};

struct pick_s {
    char *url;			/* URL of the media stream. */
    char *playing;		/* "Now Playing" */
    char *homepage;		/* Of the broadcaster. */
    char *desc;			/* Description of the broadcaster. */
    int bitrate;
    int rank;
    int min;			/* Users ... */
    int max;
};

struct urldata_s {
    char *data;
    size_t size;
};

enum {
    GENRE_DONE = 0x0001,
    GENRE_REFRESH = 0x0002,
    GENRE_CONNECT = 0x0004,
    GENRE_INIT = 0x0008,
    GENRE_FETCH = 0x0010,
    GENRE_BOOKMARK = 0x0020,
    GENRE_SECTION = 0x0040,
    GENRE_COLLAPSED = 0x0080,
    GENRE_HIDDEN = 0x0100,
    GENRE_AUTOFETCH = 0x0200,
    GENRE_SEARCH = 0x0400,
};

struct genre_rw_s {
    unsigned flags;
    int max_pages;
    int nodups;
    char buf[];
};

static struct genre_s {
    char *url;			/* The URL of the genre in the directory. */
    char *title;		/* As displayed in the menu. */
    CURL *ch;			/* CURL handle. */
    char *buf;
    struct urldata_s *urldata;	/* The HTML of the url. This gets parsed. */
    int pages;			/* Total pages for this genre. */
    int page;			/* The page "offset". */
    int max_pages;		/* Defaults from config.max_pages. */
    int nodups;			/* Defaults from config.nodups. */
    unsigned flags;		/* From above. */
    struct pick_s *picks;	/* Caching. */
    int pick;			/* Total number of picks for this genre. */
    int y;			/* The selected pick. */
    CURL *pch;			/* Handle for retrieving the playlist. */
    struct urldata_s *purldata;	/* The playlist of the select pick. */
    struct pick_s *bookmarks;	/* In this genre catagory. */
    int bookmark;
    int by;			/* The selected bookmark pick. */
} *genres;

struct ginfo_s {
    int y;
    int maxx;
    int maxy;
    int total;
} ginfo;

/* Must match the sorting enumeration in common.h. */
static char *sort_strings[] = {
    "song title", "bitrate", "server info", "connected users", "server limit",
    "rank"
};

enum {
    CONNECT, FETCH, FETCH_GENRES, DONE, BOOKMARKS
};
static struct {
    int state;
    int sort;
    int reverse_sort;
    int title;
} status;

/* Function to return the correct sorting function pointer to qsort(). */
static int (*sort_which()) (const void *s1, const void *s2);

struct message_s {
    char *line;			/* The line to display in the message box. Multi-line
				 * message boxes are supported by separating new
				 * lines with (guess) a newline character. */
    int indent;			/* The amount of space to indent wrapped lines. Lines
				 * are wrapped when the line is wider than the message
				 * box. Wrapping occurs at the previous word if
				 * possible, if not, then the word breaks at the last
				 * column of the message box. */
    int leading;		/* When 1, wrapping words are not looked for 'indent'
				 * leading characters. */
    int fd;			/* The file descriptor to write to when curses is not
				 * initialized. */
};

enum {
    SEARCH_PLAYING,
    SEARCH_DESC,
    SEARCH_HOMEPAGE,
    SEARCH_SERVER,
    SEARCH_MAX
};

struct search_s {
    int key;
    int which;
    struct genre_s *gp;
} *search;

static CURLM *cmh;
void set_default_colors(void);
void parse_rcfile(char *);
void init_color_pairs(void);
static int msg_box;
static void remove_handle(struct genre_s *g, int clean);
int set_curl_easy_opts(struct genre_s *g);

#endif
